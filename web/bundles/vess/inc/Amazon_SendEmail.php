<?php

	// RETRIEVE VARIABLES FROM URL ================================================================
	$user_date_time = date("Y-m-d H:i:s", strtotime($_POST['user_date_time']));
  $client_ip = $_SERVER['REMOTE_ADDR'];
  
	$contact_name = strtolower(urldecode($_POST['contact_name']));
	$contact_email = strtolower(urldecode($_POST['contact_email']));
	$contact_phone = strtolower(urldecode($_POST['contact_phone']));
	$contact_department = urldecode($_POST['contact_department']);
	$contact_message = strtolower(urldecode($_POST['contact_message']));
  // ============================================================================================

  $server_email = "support@hygea.net";
  
  include("AmazonMailer.php");
  
	// SEND AUTO RESPONDER =============================================================================
	$message_body = "
  <table width=\"800\" border=\"0\">
    <tr>
      <td>
        <img src='http://".$_SERVER['SERVER_NAME']."/images/Hygea-Holdings-Corporation-Logo-Email.png'><br><br>
        Thank you for contacting us.<br><br>
        A Hygea representative from our ".$contact_department." Department will be forwarded your contact information.<br><br> 
        <b>Hygea Holdings Corporation</b>
      </td>
    </tr>
  </table>
	";
	// =================================================================================================
  
	// SEND CONTACT INFO =============================================================================
	$subject = "Contact Us Request Received";
	send_email($contact_email, $server_email, $server_email, "Hygea Holdings Corporation", $subject, "<html><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><title>".$subject."</title></head><body>".$message_body."</body></html>");
	// =================================================================================================

	// SEND AUTO RESPONDER =============================================================================
	$message_body = "
  <table width=\"800\" border=\"0\">
    <tr>
      <td align='right'>Contact Name:</td>
      <td>".$contact_name."</td>
    </tr>
    <tr>
      <td align='right'>Contact Email:</td>
      <td>".$contact_email."</td>
    </tr>
    <tr>
      <td align='right'>Contact Phone:</td>
      <td>".$contact_phone."</td>
    </tr>
    <tr>
      <td align='right'>Department:</td>
      <td>".$contact_department."</td>
    </tr>
    <tr>
      <td align='right'>Contact Message:</td>
      <td>".$contact_message."</td>
    </tr>
  </table>
	";
	// =================================================================================================
	
	// SEND AUTO-RESPONDER =============================================================================
	$subject = $contact_department." Department Contact Request from: ".$contact_name." (".$contact_email.")";
	send_email("support@hygea.net", $server_email, $contact_email, "Contact Us Request", $subject, "<html><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><title>".$subject."</title></head><body>".$message_body."</body></html>");
	// =================================================================================================

?>
<?php
  
  include($_SERVER['DOCUMENT_ROOT'].'/assets/inc/global_variables.php');
  
  $module = $_GET['md'];
  
  if ( $module == "1" ) {
    $module_title = "FAQ";
    $page_to_include = "faq";
  }
  if ( $module == "2" ) {
    $module_title = "Tutorials";
    $page_to_include = "tutorials";
  }
  if ( $module == "3" ) {
    $module_title = "Contact Support";
    $page_to_include = "contact_support";
  }
  
  
  $this_page = $module_title." | Support & Coaching";
  $page_title = $this_page." | ".$_9rlabs_company_name;
  
  $breadcrumbs = "<li>Support & Coaching&nbsp;&nbsp;</li><li class=\"active\">".$module_title."</li>";

  include($_SERVER['DOCUMENT_ROOT'].'/assets/view/sections/header.php');

  include($_SERVER['DOCUMENT_ROOT'].'/assets/view/sections/navbar.php');

?>

    <div class="main-container" id="main-container">

      <div class="main-container-inner">
        <a class="menu-toggler" id="menu-toggler" href="#">
          <span class="menu-text"></span>
        </a>

        <?php include($_SERVER['DOCUMENT_ROOT'].'/assets/view/sections/sidebar.php'); ?>  

        <div class="main-content">

          <?php include($_SERVER['DOCUMENT_ROOT'].'/assets/view/sections/breadcrumbs.php'); ?> 

          <div class="page-content">

            <div class="space-12"></div>
            <div class="space-12"></div>
            <div class="space-12"></div>
            <div class="space-12"></div>
            <div class="space-12"></div>
            <div class="space-12"></div>

            <div class="page-header">
              <h1 style="color:#1c69a5;"><?php echo $module_title; ?></h1>
            </div><!-- /.page-header -->

            <div class="row">
              <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
                
                <?php include($_SERVER['DOCUMENT_ROOT'].'/assets/view/content/'.$page_to_include.'.php'); ?> 
                
                <!-- PAGE CONTENT ENDS -->
              </div><!-- /.col -->
            </div><!-- /.row -->
          </div><!-- /.page-content -->
        </div><!-- /.main-content -->

      </div><!-- /.main-container-inner -->

      <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="icon-double-angle-up icon-only bigger-110"></i>
      </a>
    </div><!-- /.main-container -->

    <?php include($_SERVER['DOCUMENT_ROOT'].'/assets/inc/javascripts.php'); ?>
    
		<script type="text/javascript">
			jQuery(function($) {
				$('.accordion').on('hide', function (e) {
					$(e.target).prev().children(0).addClass('collapsed');
				})
				$('.accordion').on('show', function (e) {
					$(e.target).prev().children(0).removeClass('collapsed');
				})
			});
		</script>    

  </body>
</html>
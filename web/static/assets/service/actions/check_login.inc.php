<?php

	// DISABLE NON INCLUDE ACCESS ====================================================================
	if ( $include_check != "2f6d8d787d9598708d96daa6140860040efb33d9" ) {
		$destination_url = $site_url;
		header( "Location: $destination_url" );
		die;	
	}
	// ===============================================================================================	

	// GET VARIABLES FROM FORM POST ==================================================================
	if ( $api_dev == true ) {
		$_9r_form_login_email = $_GET['_9r_form_login_email'];
		$_9r_form_login_password = $_GET['_9r_form_login_password'];
	} else {
		$_9r_form_login_email = $_POST['_9r_form_login_email'];
		$_9r_form_login_password = $_POST['_9r_form_login_password'];
	}
	// ===============================================================================================	

	// ENSURE BOTH USERNAME AND PASSWORD ARE ENTERED =================================================
	if (( $_9r_form_login_email == "" ) || ( $_9r_form_login_password == "" )) { 
		$output_error = json_error($action, 'Missing Variables');
		echo $output_error;
		die;
	}
	// ===============================================================================================

	// LOGIN PROCESS AND VALIDATION ==================================================================
	database_connect('amember');
	$query_mysql = "SELECT user_id, email, name_f, name_l, pass_enc FROM am_user WHERE email='".strtolower($_9r_form_login_email)."'";
	$result_mysql = mysql_query($query_mysql);
	if (mysql_num_rows($result_mysql) == 0) {
		$output_error = json_error($action, "Email doesn't exist");
		echo $output_error;
	} else {
		while ( $myrow = mysql_fetch_array($result_mysql)) {
			$pass_enc = $myrow['pass_enc'];
			if ( $pass_enc != md5($_9r_form_login_password) ) {
				echo '{"ok":false,"code":-2,"msg":"Email & password combination is incorrect"}';
			} else {
				$user_id = "\"user_id\":".$myrow['user_id'];
				$email = "\"email\":\"".$myrow['email']."\"";
				$name_f = "\"name_f\":\"".$myrow['name_f']."\"";
				$name_l = "\"name_l\":\"".$myrow['name_l']."\"";

				// CREATE SESSION AND REDIRECT =====================================================================
				session_start();
				$_SESSION['_9rlabs_user'] = $email;	
				// =================================================================================================	
	
				echo "{\"ok\":true,\"code\":\"login-success\",".$user_id.",".$email.",".$name_f.",".$name_l."}";


			}
		}
	}
	database_close();		
	// ===============================================================================================	

?>
<?php

	// DISABLE NON INCLUDE ACCESS ====================================================================
	if ( $include_check != "2f6d8d787d9598708d96daa6140860040efb33d9" ) {
		$destination_url = $site_url;
		header( "Location: $destination_url" );
		die;	
	}
	// ===============================================================================================	

	// DATABASE - OPEN CONNECTION ====================================================================
	function database_connect($db_selector) {
		
		if ( $db_selector == "amember" ) {
			$hostname = "localhost";
			$username = "ninermember";
			$password = "9rL@b$2013";
			$dbname = "ninerlabs_amember";
		} else if ( $db_selector == "ninerlabs" ) {
			$hostname = "localhost";
			$username = "ninerlabsdb";
			$password = "9rL@b$2013";
			$dbname = "ninerlabs";
		}

		$link = mysql_connect($hostname, $username, $password);
		if (!$link) {
			$output_error = json_error($action, 'Could not connect: ' . mysql_error());
			echo $output_error;
			die;
		}
		
		$db_selected = mysql_select_db($dbname, $link);
		if (!$db_selected) {
			$output_error = json_error($action, 'Could not connect: ' . mysql_error());
			echo $output_error;
			die;
		}
		
	}
	// ===============================================================================================

	// DATABASE - CLOSE CONNECTION ===================================================================
	function database_close() {
		mysql_close() or throw_ex('Caught exception in database_close: ' . mysql_error() . " - 2");
	}
	// ===============================================================================================
	
	// DO POST REQUEST ===============================================================================
	function DoPostRequest($url, $data, $optional_headers = null) {
		$params = array (
    	'http' => array (
      	'method' => 'POST',
        'content' => $data,
        'header' => "Content-type: application/x-www-form-urlencoded\r\n" .
        "Content-Length: " . strlen ( $data ) . "\r\n" 
      ) 
    );
    if ($optional_headers != null) {
    	$params ['http'] ['header'] = $optional_headers;
    }
    $ctx = stream_context_create ( $params );
    try {
      $fp = fopen ( $url, 'rb', false, $ctx );
      $response = stream_get_contents ( $fp );
    } catch ( Exception $e ) {
      echo 'Exception: ' . $e->getMessage ();
    }
    return $response;
	}
	// ===============================================================================================

	// OUTPUT ERROR MESSAGE ==========================================================================
	function json_error($api_action, $json_error_message) {
		$json_error_beg = "{\"ok\":false,\"code\":\"api_error\",\"msg\":\"";
		$json_error_end = "\"}";		
		$json_error_msg = $json_error_message;	
		return $json_error_beg.$json_error_msg.$json_error_end;
	}
	// ===============================================================================================

?>
// JavaScript Document

	function _9r_form_login() {
		
		var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
		var error_login = '';

		if ( $('#_9r_form_login_email').val() == "" ) {
			$('#_9r_form_login_email_span').addClass('has-error');
			$('#login_error_email').html('Email is required');
			error_login = true;
		} else {
			if ( $('#_9r_form_login_email').val().search(emailRegEx) == -1) {
				$('#_9r_form_login_email_span').addClass('has-error');
				$('#login_error_email').html('Email is not valid');
				error_login = true;
			}
		}
	
		if ( $('#_9r_form_login_password').val() == "" ) {
			$('#_9r_form_login_password_span').addClass('has-error');
			$('#login_error_password').html('Password is required');
			error_login = true;
		} 
	
		if ( error_login == true ) {
			return false;
		} else {
			
			remember_credentials();
			
			$('#_9r_form_login_button').css('disabled','false');
			
			var check_login = $.ajax({ type: "POST", url: "/assets/service/", data: "_9r_prime_action=check_login"  + "&_9r_prime_alpha=AZRswKa4UOqsdrcirMNsiSAAIGHT0Jr9CurhSTWkGKE2VJLaoJNEqvwvykTQlpml" + "&_9r_prime_beta=3NhFTSDR0GNLnVC05rfgyCpZCNGCclIS5MPQcW2moaNqrG69I9zXqQ6YSgUGP8IL" + "&_9r_form_login_email=" + $('#_9r_form_login_email').val() + "&_9r_form_login_password=" + $('#_9r_form_login_password').val(), async: false }).responseText;

			var login_results = jQuery.parseJSON(check_login);
		
			if ( login_results.code == "login-success" ) {

				$('#_9r_form_login_email_span').removeClass('has-error');
				$('#_9r_form_login_password_span').removeClass('has-error');
				$('#login_error_returned').html('');
				
				location.reload();
				
			} else {
				
				if ( login_results.msg == "Email doesn't exist" ) {
					$('#_9r_form_login_email_span').addClass('has-error');
				}
				if ( login_results.msg == "Email & password combination is incorrect" ) {
					$('#_9r_form_login_email_span').addClass('has-error');
					$('#_9r_form_login_password_span').addClass('has-error');
				}
				
				$('#login_error_returned').html(login_results.msg);
				
			}
		
			$('#_9r_form_login_button_process').css('disabled','true');	
					
			
		}
		
	}

	function show_box(id) {
		jQuery('.widget-box.visible').removeClass('visible');
		jQuery('#'+id).addClass('visible');
	}
	
	function remember_credentials() {
		if ($('#_9r_login_remember_me').is(':checked')) {
			// save username and password
			localStorage.usrname = $('#_9r_form_login_email').val();
			localStorage.pass = $('#_9r_form_login_password').val();
			localStorage.chkbx = $('#_9r_login_remember_me').val();
		} else {
			localStorage.usrname = '';
			localStorage.pass = '';
			localStorage.chkbx = '';
		}		
	}
	
	$(function() {

		if (localStorage.chkbx && localStorage.chkbx != '') {
			$('#_9r_login_remember_me').attr('checked', 'checked');
			$('#_9r_form_login_email').val(localStorage.usrname);
			$('#_9r_form_login_password').val(localStorage.pass);
		} else {
			$('#_9r_login_remember_me').removeAttr('checked');
			$('#_9r_form_login_email').val('');
			$('#_9r_form_login_password').val('');
		}
	
		$('#_9r_login_remember_me').click(function() {
			remember_credentials();
		});

	});

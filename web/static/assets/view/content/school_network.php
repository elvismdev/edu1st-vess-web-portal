                <h4>More than 80 schools connected for the same reason: Create a culture of learning.</h4>
                <div class="space-12"></div>
                <div class="row">
                  <div class="col-md-8">
                    <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/SchoolNetworkMapImageBorder.jpg" />
                  </div>
                </div>
                <div class="space-12"></div>
                <div class="row">
                  <div class="col-md-3 right" style="text-align:right; ">
                    <h5>Photos from around the world:</h5>
                  </div>
                  <div class="col-md-3" style="padding-top:3px;">
                    <select class="form-control" id="form-field-select-1"><option value="">Narrow Search</option><option value="AL">Argentina</option><option value="AK">Colombia</option></select>
                  </div>
                </div>
                <hr>
                <div class="row">
                
                  <div class="col-md-3">
                    <span class="profile-picture center">
                      <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/image-1.jpg" />
                    </span>
                    <h4 class="green">School Name Elementary</h4>
                    <p>Colombia</p>
                    <button class="btn btn-xs btn-danger">
                      <i class="icon-eye-open bigger-125"></i>
                      View Gallery
                    </button>                            
                    <div class="space-12"></div>
                  </div>
                
                  <div class="col-md-3">
                    <span class="profile-picture center">
                      <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/image-2.jpg" />
                    </span>
                    <h4 class="green">School Name Elementary</h4>
                    <p>Colombia</p>
                    <button class="btn btn-xs btn-danger">
                      <i class="icon-eye-open bigger-125"></i>
                      View Gallery
                    </button>                            
                    <div class="space-12"></div>
                  </div>
                
                  <div class="col-md-3">
                    <span class="profile-picture center">
                      <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/image-3.jpg" />
                    </span>
                    <h4 class="green">School Name Elementary</h4>
                    <p>Colombia</p>
                    <button class="btn btn-xs btn-danger">
                      <i class="icon-eye-open bigger-125"></i>
                      View Gallery
                    </button>                            
                    <div class="space-12"></div>
                  </div>
                
                  <div class="col-md-3">
                    <span class="profile-picture center">
                      <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/image-4.jpg" />
                    </span>
                    <h4 class="green">School Name Elementary</h4>
                    <p>Colombia</p>
                    <button class="btn btn-xs btn-danger">
                      <i class="icon-eye-open bigger-125"></i>
                      View Gallery
                    </button>                            
                    <div class="space-12"></div>
                  </div>
                
                  <div class="col-md-3">
                    <span class="profile-picture center">
                      <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/image-5.jpg" />
                    </span>
                    <h4 class="green">School Name Elementary</h4>
                    <p>Colombia</p>
                    <button class="btn btn-xs btn-danger">
                      <i class="icon-eye-open bigger-125"></i>
                      View Gallery
                    </button>                            
                    <div class="space-12"></div>
                  </div>
                
                  <div class="col-md-3">
                    <span class="profile-picture center">
                      <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/image-6.jpg" />
                    </span>
                    <h4 class="green">School Name Elementary</h4>
                    <p>Colombia</p>
                    <button class="btn btn-xs btn-danger">
                      <i class="icon-eye-open bigger-125"></i>
                      View Gallery
                    </button>                            
                    <div class="space-12"></div>
                  </div>
                
                  <div class="col-md-3">
                    <span class="profile-picture center">
                      <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/image-7.jpg" />
                    </span>
                    <h4 class="green">School Name Elementary</h4>
                    <p>Colombia</p>
                    <button class="btn btn-xs btn-danger">
                      <i class="icon-eye-open bigger-125"></i>
                      View Gallery
                    </button>                            
                    <div class="space-12"></div>
                  </div>
                
                  <div class="col-md-3">
                    <span class="profile-picture center">
                      <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/image-8.jpg" />
                    </span>
                    <h4 class="green">School Name Elementary</h4>
                    <p>Colombia</p>
                    <button class="btn btn-xs btn-danger">
                      <i class="icon-eye-open bigger-125"></i>
                      View Gallery
                    </button>                            
                    <div class="space-12"></div>
                  </div>
                  
                </div>


                

                
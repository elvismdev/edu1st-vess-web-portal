
                <div class="space-12"></div>

                <div class="tabbable">
                  <ul class="nav nav-tabs padding-18 tab-size-bigger" id="myTab">
                    <li class="active">
                      <a data-toggle="tab" href="#faq-tab-1">
                        <i class="green icon-check bigger-120"></i>
                        Products
                      </a>
                    </li>

                    <li>
                      <a data-toggle="tab" href="#">
                        <i class="green icon-calendar bigger-120"></i>
                        Planners
                      </a>
                    </li>

                    <li>
                      <a data-toggle="tab" href="#">
                        <i class="green icon-laptop bigger-120"></i>
                        Webinars
                      </a>
                    </li>

                    <li>
                      <a data-toggle="tab" href="#">
                        <i class="green icon-lightbulb bigger-120"></i>
                        Thinking Tools
                      </a>
                    </li>
                    
                    <li>
                      <a data-toggle="tab" href="#">
                        <i class="green icon-globe bigger-120"></i>
                        International Institute
                      </a>
                    </li>
                    
                    <li>
                      <a data-toggle="tab" href="#">
                        <i class="green icon-book bigger-120"></i>
                        Online Courses
                      </a>
                    </li>
                    
                    <li>
                      <a data-toggle="tab" href="#">
                        <i class="green icon-cogs bigger-120"></i>
                        Thinking Routines
                      </a>
                    </li>
                    
                    <li>
                      <a data-toggle="tab" href="#">
                        <i class="green icon-desktop bigger-120"></i>
                        Webs
                      </a>
                    </li>                                                                                

                  </ul>

                  <div class="tab-content no-border padding-24">

                    <div id="faq-tab-1" class="tab-pane fade in active">
                      <div class="row">
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/image-1.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/image-3.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/image-4.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/image-5.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/image-6.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/image-7.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/image-8.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                      </div>
                    </div>

                    <div id="faq-tab-2" class="tab-pane fade">
                      <div class="row">
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                      </div>
                    </div>

                    <div id="faq-tab-3" class="tab-pane fade">
                      <div class="row">
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                      </div>
                    </div>

                    <div id="faq-tab-4" class="tab-pane fade">
                      <div class="row">
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                      </div>
                    </div>

                    <div id="faq-tab-5" class="tab-pane fade">
                      <div class="row">
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                      </div>
                    </div>

                    <div id="faq-tab-6" class="tab-pane fade">
                      <div class="row">
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                      </div>
                    </div>

                    <div id="faq-tab-7" class="tab-pane fade">
                      <div class="row">
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                      </div>
                    </div>

                    <div id="faq-tab-8" class="tab-pane fade">
                      <div class="row">
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                        <div class="col-md-3">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <h4 class="green">Product Name</h4>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                          <div class="row">
                            <div class="col-xs-6 left"><h4 style="margin-top:3px;">$29.99</h4></div>
                            <div class="col-xs-6 left">
                              <button class="btn btn-xs btn-primary">
                                <i class="icon-plus bigger-125"></i>
                                Add to Cart
                              </button>                            
                            </div>
                          </div>
                          <div class="space-12"></div>
                        </div>
                      
                      </div>
                    </div>

                  </div>
                </div>
                
                
                
                <div class="row">
                  
                  <div class="col-xs-6"></div>
                
                  <div class="col-xs-6 align-right">
                  
                    <button class="btn btn-success">Filter Option</button> <button class="btn btn-success">Curriculum Map</button> <button class="btn btn-success">How it Works</button>
                  
                  </div>
                
                </div>

                <div class="space-12"></div>

                <div class="tabbable">
                  <ul class="nav nav-tabs padding-18 tab-size-bigger" id="myTab">
                    <li class="active">
                      <a data-toggle="tab" href="#faq-tab-1">
                        <i class="green icon-fire bigger-120"></i>
                        Philosophy
                      </a>
                    </li>

                    <li>
                      <a data-toggle="tab" href="#">
                        <i class="green icon-bookmark bigger-120"></i>
                        Themes
                      </a>
                    </li>

                    <li>
                      <a data-toggle="tab" href="#">
                        <i class="green icon-asterisk bigger-120"></i>
                        Art Focus
                      </a>
                    </li>

                    <li>
                      <a data-toggle="tab" href="#">
                        <i class="green icon-flag bigger-120"></i>
                        Country
                      </a>
                    </li>

                  </ul>

                  <div class="tab-content no-border padding-24">
                    <div id="faq-tab-1" class="tab-pane fade in active">

                      <div class="col-xs-3 center">
                        <span class="profile-picture center">
                          <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/image-1.jpg" />
                        </span>
                        <div class="space-4"></div>
                        <div class="label label-info label-xlg arrowed-in arrowed-in-right">
                          <div class="inline center">
                            <span class="user-title-label white">Theme 1 </span>
                          </div>
                        </div>
                        <div class="space-8"></div> 
                        <div class="form-group">                              
                          <select class="form-control" id="form-field-select-1">
                            <option value="">Select Period</option>
                            <option value="AL">Period 1</option>
                            <option value="AK">Period 2</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <select class="form-control" id="form-field-select-1">
                            <option value="">Select Planner</option>
                            <option value="AL">Planner 1</option>
                            <option value="AK">Planner 2</option>
                          </select>
                        </div>
                        <div class="space-12"></div>
                      </div>

                      <div class="col-xs-3 center">
                        <span class="profile-picture center">
                          <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/image-1.jpg" />
                        </span>
                        <div class="space-4"></div>
                        <div class="label label-info label-xlg arrowed-in arrowed-in-right">
                          <div class="inline center">
                            <span class="user-title-label white">Theme 1 </span>
                          </div>
                        </div>
                        <div class="space-8"></div> 
                        <div class="form-group">                              
                          <select class="form-control" id="form-field-select-1">
                            <option value="">Select Period</option>
                            <option value="AL">Period 1</option>
                            <option value="AK">Period 2</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <select class="form-control" id="form-field-select-1">
                            <option value="">Select Planner</option>
                            <option value="AL">Planner 1</option>
                            <option value="AK">Planner 2</option>
                          </select>
                        </div>
                        <div class="space-12"></div>
                      </div>

                      <div class="col-xs-3 center">
                        <span class="profile-picture center">
                          <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/image-1.jpg" />
                        </span>
                        <div class="space-4"></div>
                        <div class="label label-info label-xlg arrowed-in arrowed-in-right">
                          <div class="inline center">
                            <span class="user-title-label white">Theme 1 </span>
                          </div>
                        </div>
                        <div class="space-8"></div> 
                        <div class="form-group">                              
                          <select class="form-control" id="form-field-select-1">
                            <option value="">Select Period</option>
                            <option value="AL">Period 1</option>
                            <option value="AK">Period 2</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <select class="form-control" id="form-field-select-1">
                            <option value="">Select Planner</option>
                            <option value="AL">Planner 1</option>
                            <option value="AK">Planner 2</option>
                          </select>
                        </div>
                        <div class="space-12"></div>
                      </div>

                      <div class="col-xs-3 center">
                        <span class="profile-picture center">
                          <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/image-1.jpg" />
                        </span>
                        <div class="space-4"></div>
                        <div class="label label-info label-xlg arrowed-in arrowed-in-right">
                          <div class="inline center">
                            <span class="user-title-label white">Theme 1 </span>
                          </div>
                        </div>
                        <div class="space-8"></div> 
                        <div class="form-group">                              
                          <select class="form-control" id="form-field-select-1">
                            <option value="">Select Period</option>
                            <option value="AL">Period 1</option>
                            <option value="AK">Period 2</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <select class="form-control" id="form-field-select-1">
                            <option value="">Select Planner</option>
                            <option value="AL">Planner 1</option>
                            <option value="AK">Planner 2</option>
                          </select>
                        </div>
                        <div class="space-12"></div>
                      </div>

                      <div class="col-xs-3 center">
                        <span class="profile-picture center">
                          <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/image-1.jpg" />
                        </span>
                        <div class="space-4"></div>
                        <div class="label label-info label-xlg arrowed-in arrowed-in-right">
                          <div class="inline center">
                            <span class="user-title-label white">Theme 1 </span>
                          </div>
                        </div>
                        <div class="space-8"></div> 
                        <div class="form-group">                              
                          <select class="form-control" id="form-field-select-1">
                            <option value="">Select Period</option>
                            <option value="AL">Period 1</option>
                            <option value="AK">Period 2</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <select class="form-control" id="form-field-select-1">
                            <option value="">Select Planner</option>
                            <option value="AL">Planner 1</option>
                            <option value="AK">Planner 2</option>
                          </select>
                        </div>
                        <div class="space-12"></div>
                      </div>

                      <div class="col-xs-3 center">
                        <span class="profile-picture center">
                          <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/image-1.jpg" />
                        </span>
                        <div class="space-4"></div>
                        <div class="label label-info label-xlg arrowed-in arrowed-in-right">
                          <div class="inline center">
                            <span class="user-title-label white">Theme 1 </span>
                          </div>
                        </div>
                        <div class="space-8"></div> 
                        <div class="form-group">                              
                          <select class="form-control" id="form-field-select-1">
                            <option value="">Select Period</option>
                            <option value="AL">Period 1</option>
                            <option value="AK">Period 2</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <select class="form-control" id="form-field-select-1">
                            <option value="">Select Planner</option>
                            <option value="AL">Planner 1</option>
                            <option value="AK">Planner 2</option>
                          </select>
                        </div>
                        <div class="space-12"></div>
                      </div>

                      <div class="col-xs-3 center">
                        <span class="profile-picture center">
                          <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/image-1.jpg" />
                        </span>
                        <div class="space-4"></div>
                        <div class="label label-info label-xlg arrowed-in arrowed-in-right">
                          <div class="inline center">
                            <span class="user-title-label white">Theme 1 </span>
                          </div>
                        </div>
                        <div class="space-8"></div> 
                        <div class="form-group">                              
                          <select class="form-control" id="form-field-select-1">
                            <option value="">Select Period</option>
                            <option value="AL">Period 1</option>
                            <option value="AK">Period 2</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <select class="form-control" id="form-field-select-1">
                            <option value="">Select Planner</option>
                            <option value="AL">Planner 1</option>
                            <option value="AK">Planner 2</option>
                          </select>
                        </div>
                        <div class="space-12"></div>
                      </div>

                      <div class="col-xs-3 center">
                        <span class="profile-picture center">
                          <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/image-1.jpg" />
                        </span>
                        <div class="space-4"></div>
                        <div class="label label-info label-xlg arrowed-in arrowed-in-right">
                          <div class="inline center">
                            <span class="user-title-label white">Theme 1 </span>
                          </div>
                        </div>
                        <div class="space-8"></div> 
                        <div class="form-group">                              
                          <select class="form-control" id="form-field-select-1">
                            <option value="">Select Period</option>
                            <option value="AL">Period 1</option>
                            <option value="AK">Period 2</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <select class="form-control" id="form-field-select-1">
                            <option value="">Select Planner</option>
                            <option value="AL">Planner 1</option>
                            <option value="AK">Planner 2</option>
                          </select>
                        </div>
                        <div class="space-12"></div>
                      </div>


                    </div>

                    <div id="faq-tab-2" class="tab-pane fade">

                      <div class="row">
                      
                        <div class="col-xs-3 center">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <div class="space-4"></div>
                          <div class="label label-info label-xlg arrowed-in arrowed-in-right">
                            <div class="center">
                              <span class="user-title-label white">Theme 1 </span>
                            </div>
                            <div class="space-8"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Period</option>
                              <option value="AL">Period 1</option>
                              <option value="AK">Period 2</option>
                            </select>
                            <div class="space-4"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Planner</option>
                              <option value="AL">Planner 1</option>
                              <option value="AK">Planner 2</option>
                            </select>
                            <div class="space-12"></div>
                          </div>
                        </div>
                      
                        <div class="col-xs-3 center">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <div class="space-4"></div>
                          <div class="label label-info label-xlg arrowed-in arrowed-in-right">
                            <div class="center">
                              <span class="user-title-label white">Theme 1 </span>
                            </div>
                            <div class="space-8"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Period</option>
                              <option value="AL">Period 1</option>
                              <option value="AK">Period 2</option>
                            </select>
                            <div class="space-4"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Planner</option>
                              <option value="AL">Planner 1</option>
                              <option value="AK">Planner 2</option>
                            </select>
                            <div class="space-12"></div>
                          </div>
                        </div>
                      
                        <div class="col-xs-3 center">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <div class="space-4"></div>
                          <div class="label label-info label-xlg arrowed-in arrowed-in-right">
                            <div class="center">
                              <span class="user-title-label white">Theme 1 </span>
                            </div>
                            <div class="space-8"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Period</option>
                              <option value="AL">Period 1</option>
                              <option value="AK">Period 2</option>
                            </select>
                            <div class="space-4"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Planner</option>
                              <option value="AL">Planner 1</option>
                              <option value="AK">Planner 2</option>
                            </select>
                            <div class="space-12"></div>
                          </div>
                        </div>
                      
                        <div class="col-xs-3 center">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <div class="space-4"></div>
                          <div class="label label-info label-xlg arrowed-in arrowed-in-right">
                            <div class="center">
                              <span class="user-title-label white">Theme 1 </span>
                            </div>
                            <div class="space-8"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Period</option>
                              <option value="AL">Period 1</option>
                              <option value="AK">Period 2</option>
                            </select>
                            <div class="space-4"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Planner</option>
                              <option value="AL">Planner 1</option>
                              <option value="AK">Planner 2</option>
                            </select>
                            <div class="space-12"></div>
                          </div>
                        </div>
                      
                        <div class="col-xs-3 center">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <div class="space-4"></div>
                          <div class="label label-info label-xlg arrowed-in arrowed-in-right">
                            <div class="center">
                              <span class="user-title-label white">Theme 1 </span>
                            </div>
                            <div class="space-8"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Period</option>
                              <option value="AL">Period 1</option>
                              <option value="AK">Period 2</option>
                            </select>
                            <div class="space-4"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Planner</option>
                              <option value="AL">Planner 1</option>
                              <option value="AK">Planner 2</option>
                            </select>
                            <div class="space-12"></div>
                          </div>
                        </div>
                      
                        <div class="col-xs-3 center">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <div class="space-4"></div>
                          <div class="label label-info label-xlg arrowed-in arrowed-in-right">
                            <div class="center">
                              <span class="user-title-label white">Theme 1 </span>
                            </div>
                            <div class="space-8"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Period</option>
                              <option value="AL">Period 1</option>
                              <option value="AK">Period 2</option>
                            </select>
                            <div class="space-4"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Planner</option>
                              <option value="AL">Planner 1</option>
                              <option value="AK">Planner 2</option>
                            </select>
                            <div class="space-12"></div>
                          </div>
                        </div>
                      
                        <div class="col-xs-3 center">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <div class="space-4"></div>
                          <div class="label label-info label-xlg arrowed-in arrowed-in-right">
                            <div class="center">
                              <span class="user-title-label white">Theme 1 </span>
                            </div>
                            <div class="space-8"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Period</option>
                              <option value="AL">Period 1</option>
                              <option value="AK">Period 2</option>
                            </select>
                            <div class="space-4"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Planner</option>
                              <option value="AL">Planner 1</option>
                              <option value="AK">Planner 2</option>
                            </select>
                            <div class="space-12"></div>
                          </div>
                        </div>
                      
                        <div class="col-xs-3 center">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <div class="space-4"></div>
                          <div class="label label-info label-xlg arrowed-in arrowed-in-right">
                            <div class="center">
                              <span class="user-title-label white">Theme 1 </span>
                            </div>
                            <div class="space-8"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Period</option>
                              <option value="AL">Period 1</option>
                              <option value="AK">Period 2</option>
                            </select>
                            <div class="space-4"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Planner</option>
                              <option value="AL">Planner 1</option>
                              <option value="AK">Planner 2</option>
                            </select>
                            <div class="space-12"></div>
                          </div>
                        </div>
                      
                      </div>

                    </div>

                    <div id="faq-tab-3" class="tab-pane fade">

                      <div class="row">
                      
                        <div class="col-xs-3 center">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <div class="space-4"></div>
                          <div class="label label-info label-xlg arrowed-in arrowed-in-right">
                            <div class="center">
                              <span class="user-title-label white">Theme 1 </span>
                            </div>
                            <div class="space-8"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Period</option>
                              <option value="AL">Period 1</option>
                              <option value="AK">Period 2</option>
                            </select>
                            <div class="space-4"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Planner</option>
                              <option value="AL">Planner 1</option>
                              <option value="AK">Planner 2</option>
                            </select>
                            <div class="space-12"></div>
                          </div>
                        </div>
                      
                        <div class="col-xs-3 center">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <div class="space-4"></div>
                          <div class="label label-info label-xlg arrowed-in arrowed-in-right">
                            <div class="center">
                              <span class="user-title-label white">Theme 1 </span>
                            </div>
                            <div class="space-8"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Period</option>
                              <option value="AL">Period 1</option>
                              <option value="AK">Period 2</option>
                            </select>
                            <div class="space-4"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Planner</option>
                              <option value="AL">Planner 1</option>
                              <option value="AK">Planner 2</option>
                            </select>
                            <div class="space-12"></div>
                          </div>
                        </div>
                      
                        <div class="col-xs-3 center">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <div class="space-4"></div>
                          <div class="label label-info label-xlg arrowed-in arrowed-in-right">
                            <div class="center">
                              <span class="user-title-label white">Theme 1 </span>
                            </div>
                            <div class="space-8"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Period</option>
                              <option value="AL">Period 1</option>
                              <option value="AK">Period 2</option>
                            </select>
                            <div class="space-4"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Planner</option>
                              <option value="AL">Planner 1</option>
                              <option value="AK">Planner 2</option>
                            </select>
                            <div class="space-12"></div>
                          </div>
                        </div>
                      
                        <div class="col-xs-3 center">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <div class="space-4"></div>
                          <div class="label label-info label-xlg arrowed-in arrowed-in-right">
                            <div class="center">
                              <span class="user-title-label white">Theme 1 </span>
                            </div>
                            <div class="space-8"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Period</option>
                              <option value="AL">Period 1</option>
                              <option value="AK">Period 2</option>
                            </select>
                            <div class="space-4"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Planner</option>
                              <option value="AL">Planner 1</option>
                              <option value="AK">Planner 2</option>
                            </select>
                            <div class="space-12"></div>
                          </div>
                        </div>
                      
                        <div class="col-xs-3 center">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <div class="space-4"></div>
                          <div class="label label-info label-xlg arrowed-in arrowed-in-right">
                            <div class="center">
                              <span class="user-title-label white">Theme 1 </span>
                            </div>
                            <div class="space-8"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Period</option>
                              <option value="AL">Period 1</option>
                              <option value="AK">Period 2</option>
                            </select>
                            <div class="space-4"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Planner</option>
                              <option value="AL">Planner 1</option>
                              <option value="AK">Planner 2</option>
                            </select>
                            <div class="space-12"></div>
                          </div>
                        </div>
                      
                        <div class="col-xs-3 center">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <div class="space-4"></div>
                          <div class="label label-info label-xlg arrowed-in arrowed-in-right">
                            <div class="center">
                              <span class="user-title-label white">Theme 1 </span>
                            </div>
                            <div class="space-8"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Period</option>
                              <option value="AL">Period 1</option>
                              <option value="AK">Period 2</option>
                            </select>
                            <div class="space-4"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Planner</option>
                              <option value="AL">Planner 1</option>
                              <option value="AK">Planner 2</option>
                            </select>
                            <div class="space-12"></div>
                          </div>
                        </div>
                      
                        <div class="col-xs-3 center">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <div class="space-4"></div>
                          <div class="label label-info label-xlg arrowed-in arrowed-in-right">
                            <div class="center">
                              <span class="user-title-label white">Theme 1 </span>
                            </div>
                            <div class="space-8"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Period</option>
                              <option value="AL">Period 1</option>
                              <option value="AK">Period 2</option>
                            </select>
                            <div class="space-4"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Planner</option>
                              <option value="AL">Planner 1</option>
                              <option value="AK">Planner 2</option>
                            </select>
                            <div class="space-12"></div>
                          </div>
                        </div>
                      
                        <div class="col-xs-3 center">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <div class="space-4"></div>
                          <div class="label label-info label-xlg arrowed-in arrowed-in-right">
                            <div class="center">
                              <span class="user-title-label white">Theme 1 </span>
                            </div>
                            <div class="space-8"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Period</option>
                              <option value="AL">Period 1</option>
                              <option value="AK">Period 2</option>
                            </select>
                            <div class="space-4"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Planner</option>
                              <option value="AL">Planner 1</option>
                              <option value="AK">Planner 2</option>
                            </select>
                            <div class="space-12"></div>
                          </div>
                        </div>
                      
                      </div>

                    </div>

                    <div id="faq-tab-4" class="tab-pane fade">

                      <div class="row">
                      
                        <div class="col-xs-3 center">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <div class="space-4"></div>
                          <div class="label label-info label-xlg arrowed-in arrowed-in-right">
                            <div class="center">
                              <span class="user-title-label white">Theme 1 </span>
                            </div>
                            <div class="space-8"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Period</option>
                              <option value="AL">Period 1</option>
                              <option value="AK">Period 2</option>
                            </select>
                            <div class="space-4"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Planner</option>
                              <option value="AL">Planner 1</option>
                              <option value="AK">Planner 2</option>
                            </select>
                            <div class="space-12"></div>
                          </div>
                        </div>
                      
                        <div class="col-xs-3 center">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <div class="space-4"></div>
                          <div class="label label-info label-xlg arrowed-in arrowed-in-right">
                            <div class="center">
                              <span class="user-title-label white">Theme 1 </span>
                            </div>
                            <div class="space-8"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Period</option>
                              <option value="AL">Period 1</option>
                              <option value="AK">Period 2</option>
                            </select>
                            <div class="space-4"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Planner</option>
                              <option value="AL">Planner 1</option>
                              <option value="AK">Planner 2</option>
                            </select>
                            <div class="space-12"></div>
                          </div>
                        </div>
                      
                        <div class="col-xs-3 center">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <div class="space-4"></div>
                          <div class="label label-info label-xlg arrowed-in arrowed-in-right">
                            <div class="center">
                              <span class="user-title-label white">Theme 1 </span>
                            </div>
                            <div class="space-8"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Period</option>
                              <option value="AL">Period 1</option>
                              <option value="AK">Period 2</option>
                            </select>
                            <div class="space-4"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Planner</option>
                              <option value="AL">Planner 1</option>
                              <option value="AK">Planner 2</option>
                            </select>
                            <div class="space-12"></div>
                          </div>
                        </div>
                      
                        <div class="col-xs-3 center">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <div class="space-4"></div>
                          <div class="label label-info label-xlg arrowed-in arrowed-in-right">
                            <div class="center">
                              <span class="user-title-label white">Theme 1 </span>
                            </div>
                            <div class="space-8"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Period</option>
                              <option value="AL">Period 1</option>
                              <option value="AK">Period 2</option>
                            </select>
                            <div class="space-4"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Planner</option>
                              <option value="AL">Planner 1</option>
                              <option value="AK">Planner 2</option>
                            </select>
                            <div class="space-12"></div>
                          </div>
                        </div>
                      
                        <div class="col-xs-3 center">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <div class="space-4"></div>
                          <div class="label label-info label-xlg arrowed-in arrowed-in-right">
                            <div class="center">
                              <span class="user-title-label white">Theme 1 </span>
                            </div>
                            <div class="space-8"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Period</option>
                              <option value="AL">Period 1</option>
                              <option value="AK">Period 2</option>
                            </select>
                            <div class="space-4"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Planner</option>
                              <option value="AL">Planner 1</option>
                              <option value="AK">Planner 2</option>
                            </select>
                            <div class="space-12"></div>
                          </div>
                        </div>
                      
                        <div class="col-xs-3 center">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <div class="space-4"></div>
                          <div class="label label-info label-xlg arrowed-in arrowed-in-right">
                            <div class="center">
                              <span class="user-title-label white">Theme 1 </span>
                            </div>
                            <div class="space-8"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Period</option>
                              <option value="AL">Period 1</option>
                              <option value="AK">Period 2</option>
                            </select>
                            <div class="space-4"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Planner</option>
                              <option value="AL">Planner 1</option>
                              <option value="AK">Planner 2</option>
                            </select>
                            <div class="space-12"></div>
                          </div>
                        </div>
                      
                        <div class="col-xs-3 center">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <div class="space-4"></div>
                          <div class="label label-info label-xlg arrowed-in arrowed-in-right">
                            <div class="center">
                              <span class="user-title-label white">Theme 1 </span>
                            </div>
                            <div class="space-8"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Period</option>
                              <option value="AL">Period 1</option>
                              <option value="AK">Period 2</option>
                            </select>
                            <div class="space-4"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Planner</option>
                              <option value="AL">Planner 1</option>
                              <option value="AK">Planner 2</option>
                            </select>
                            <div class="space-12"></div>
                          </div>
                        </div>
                      
                        <div class="col-xs-3 center">
                          <span class="profile-picture center">
                            <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="/assets/images/gallery/image-2.jpg" />
                          </span>
                          <div class="space-4"></div>
                          <div class="label label-info label-xlg arrowed-in arrowed-in-right">
                            <div class="center">
                              <span class="user-title-label white">Theme 1 </span>
                            </div>
                            <div class="space-8"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Period</option>
                              <option value="AL">Period 1</option>
                              <option value="AK">Period 2</option>
                            </select>
                            <div class="space-4"></div>      
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Planner</option>
                              <option value="AL">Planner 1</option>
                              <option value="AK">Planner 2</option>
                            </select>
                            <div class="space-12"></div>
                          </div>
                        </div>
                      
                      </div>

                    </div>

                  </div>
                </div>
                
                


                <div class="space-12"></div>

                <div class="tabbable">
                  <ul class="nav nav-tabs padding-18 tab-size-bigger" id="myTab">
                    <li class="active">
                      <a data-toggle="tab" href="#faq-tab-1">
                        <i class="green icon-camera bigger-120"></i>
                        Photos
                      </a>
                    </li>

                    <li>
                      <a data-toggle="tab" href="#">
                        <i class="green icon-facetime-video bigger-120"></i>
                        Videos
                      </a>
                    </li>

                    <li>
                      <a data-toggle="tab" href="#">
                        <i class="green icon-inbox bigger-120"></i>
                        Documents/Articles
                      </a>
                    </li>

                    <li>
                      <a data-toggle="tab" href="#">
                        <i class="green icon-external-link bigger-120"></i>
                        Webpages/Links
                      </a>
                    </li>

                  </ul>

                  <div class="tab-content no-border padding-24">
                    <div id="faq-tab-1" class="tab-pane fade in active">

                      <form class="form-horizontal" role="form">
                        <button class="btn btn-sm btn-danger">
                          <i class="icon-cloud-upload bigger-125"></i>
                          Upload Photo
                        </button>  
  
                        <button class="btn btn-sm btn-danger">
                          <i class="icon-th-large bigger-125"></i>
                          Create Album
                        </button>  
  
                        <div class="space-12"></div>
                        
                        <div class="row">
                          <div class="col-xs-8">
                            <div class="input-group">
                              <input type="text" class="form-control search-query" placeholder="What's on your mind..." />
                              <span class="input-group-btn">
                                <button type="button" class="btn btn-sm btn-primary">
                                  <i class="icon-arrow-right icon-on-right bigger-110"></i>
                                  Post
                                </button>
                              </span>
                            </div>
                          </div>
                        </div>
                        <div class="space-6"></div>
                        <div class="row">
                          <div class="col-xs-6" style="padding-top:2px;">
                            <select multiple="" class="width-100 chosen-select" id="form-field-select-4" data-placeholder="Tag People / School">
                              <option value="">&nbsp;</option>
                              <option value="AL">Doral</option>
                              <option value="AK">Kendall</option>
                              <option value="AZ">Miami</option>
                              <option value="AR">Miami Beach</option>
                              <option value="CA">Pembroke Pines</option>
                              <option value="CO">Weston</option>
                            </select>
                          </div>
                          <div class="col-md-2">
                            <select class="form-control" id="form-field-select-1">
                              <option value="">Select Audience</option>
                              <option value="AL">Private</option>
                              <option value="AK">Public</option>
                            </select>
                          </div>
                        </div>
                        <hr>
                        <div class="row">
                          <div class="col-md-8 right">
                            <div class="btn-group">
                              <button data-toggle="dropdown" class="btn btn-sm btn-success dropdown-toggle right">
                                Sort by
                                <span class="icon-caret-down icon-on-right"></span>
                              </button>
      
                              <ul class="dropdown-menu dropdown-default">
                                <li>
                                  <a href="#">Most Recent</a>
                                </li>
      
                                <li>
                                  <a href="#">Top Themes</a>
                                </li>
                              </ul>
                            </div><!-- /btn-group -->

                            
                          </div>
                        </div>
                        <hr>
                        <div class="row">
                          <div class="col-md-8">
                            <div class="dialogs">
                              <div class="itemdiv dialogdiv">
                                <div class="user">
                                  <img alt="Santiago's Avatar" src="/assets/avatars/santiago.png" />
                                </div>
                            
                                <div class="body">
                                  <div class="time">
                                    <i class="icon-time"></i>
                                    <span class="green">4 sec</span>
                                  </div>
                            
                                  <div class="name">
                                    Santiago | Weston, Florida
                                  </div>
                                  <div class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque commodo massa sed ipsum porttitor facilisis.</div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-4">
                            <span class="profile-picture center">
                              <img class="editable img-responsive" src="/assets/images/image-1.jpg" style="width:300px;" />
                            </span>
                          </div>
                          <div class="col-md-4">
                            <h4 class="green">Theme Name</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque commodo massa sed ipsum porttitor facilisis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque commodo massa sed ipsum porttitor facilisis. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            <div class="row">
                              <div class="col-md-6">
                                <p><i class="icon-star orange"></i><i class="icon-star orange"></i><i class="icon-star orange"></i><i class="icon-star orange"></i><i class="icon-star gray"></i></p>
                                <p><a href="#">Like</a> | <a href="#">Comment</a></p>
                              </div>
                              <div class="col-md-6">
                                <select class="form-control" id="form-field-select-1">
                                  <option value="">Select Period</option>
                                  <option value="AL">Period 1</option>
                                  <option value="AK">Period 2</option>
                                </select>
                                <div class="space-4"></div>      
                                <select class="form-control" id="form-field-select-1">
                                  <option value="">Select Planner</option>
                                  <option value="AL">Planner 1</option>
                                  <option value="AK">Planner 2</option>
                                </select>    
                              </div>
                            </div>
                          </div>
                          
                        </div>
                        <div class="row">
                          <div class="col-md-8">
                            <div class="widget-box ">
                              <div class="widget-header">
                                <h4 class="lighter smaller">
                                  <i class="icon-comment blue"></i>
                                  Comments
                                </h4>
                              </div>
                            
                              <div class="widget-body">
                                <div class="widget-main no-padding">
                                  <div class="dialogs">
                                    <div class="itemdiv dialogdiv">
                                      <div class="user">
                                        <img alt="Alexa's Avatar" src="/assets/avatars/avatar1.png" />
                                      </div>
                            
                                      <div class="body">
                                        <div class="time">
                                          <i class="icon-time"></i>
                                          <span class="green">4 sec</span>
                                        </div>
                            
                                        <div class="name">
                                          <a href="#">Alexa</a>
                                        </div>
                                        <div class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
                            
                                        <div class="tools">
                                          <a href="#" class="btn btn-minier btn-info">
                                            <i class="icon-only icon-share-alt"></i>
                                          </a>
                                        </div>
                                      </div>
                                    </div>
                            
                                    <div class="itemdiv dialogdiv">
                                      <div class="user">
                                        <img alt="John's Avatar" src="/assets/avatars/avatar.png" />
                                      </div>
                            
                                      <div class="body">
                                        <div class="time">
                                          <i class="icon-time"></i>
                                          <span class="blue">38 sec</span>
                                        </div>
                            
                                        <div class="name">
                                          <a href="#">John</a>
                                        </div>
                                        <div class="text">Raw denim you probably haven&#39;t heard of them jean shorts Austin.</div>
                            
                                        <div class="tools">
                                          <a href="#" class="btn btn-minier btn-info">
                                            <i class="icon-only icon-share-alt"></i>
                                          </a>
                                        </div>
                                      </div>
                                    </div>
                            
                                    <div class="itemdiv dialogdiv">
                                      <div class="user">
                                        <img alt="Bob's Avatar" src="/assets/avatars/user.jpg" />
                                      </div>
                            
                                      <div class="body">
                                        <div class="time">
                                          <i class="icon-time"></i>
                                          <span class="orange">2 min</span>
                                        </div>
                            
                                        <div class="name">
                                          <a href="#">Bob</a>
                                        </div>
                                        <div class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque commodo massa sed ipsum porttitor facilisis.</div>
                            
                                        <div class="tools">
                                          <a href="#" class="btn btn-minier btn-info">
                                            <i class="icon-only icon-share-alt"></i>
                                          </a>
                                        </div>
                                      </div>
                                    </div>
                            
                                  </div>
                            
                                </div><!-- /widget-main -->
                              </div><!-- /widget-body -->
                            </div><!-- /widget-box -->
                          </div>
                        </div>

                        <hr>
                        <div class="row">
                          <div class="col-md-8">
                            <div class="dialogs">
                              <div class="itemdiv dialogdiv">
                                <div class="user">
                                  <img alt="Santiago's Avatar" src="/assets/avatars/santiago.png" />
                                </div>
                            
                                <div class="body">
                                  <div class="time">
                                    <i class="icon-time"></i>
                                    <span class="green">4 sec</span>
                                  </div>
                            
                                  <div class="name">
                                    Santiago | Weston, Florida
                                  </div>
                                  <div class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque commodo massa sed ipsum porttitor facilisis.</div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-4">
                            <span class="profile-picture center">
                              <img class="editable img-responsive" src="/assets/images/image-2.jpg" style="width:300px;" />
                            </span>
                          </div>
                          <div class="col-md-4">
                            <h4 class="green">Theme Name</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque commodo massa sed ipsum porttitor facilisis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque commodo massa sed ipsum porttitor facilisis. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            <div class="row">
                              <div class="col-md-6">
                                <p><i class="icon-star orange"></i><i class="icon-star orange"></i><i class="icon-star orange"></i><i class="icon-star orange"></i><i class="icon-star gray"></i></p>
                                <p><a href="#">Like</a> | <a href="#">Comment</a></p>
                              </div>
                              <div class="col-md-6">
                                <select class="form-control" id="form-field-select-1">
                                  <option value="">Select Period</option>
                                  <option value="AL">Period 1</option>
                                  <option value="AK">Period 2</option>
                                </select>
                                <div class="space-4"></div>      
                                <select class="form-control" id="form-field-select-1">
                                  <option value="">Select Planner</option>
                                  <option value="AL">Planner 1</option>
                                  <option value="AK">Planner 2</option>
                                </select>    
                              </div>
                            </div>
                          </div>
                          
                        </div>
                        <div class="row">
                          <div class="col-md-8">
                            <div class="widget-box ">
                              <div class="widget-header">
                                <h4 class="lighter smaller">
                                  <i class="icon-comment blue"></i>
                                  Comments
                                </h4>
                              </div>
                            
                              <div class="widget-body">
                                <div class="widget-main no-padding">
                                  <div class="dialogs">
                                    <div class="itemdiv dialogdiv">
                                      <div class="user">
                                        <img alt="Alexa's Avatar" src="/assets/avatars/avatar1.png" />
                                      </div>
                            
                                      <div class="body">
                                        <div class="time">
                                          <i class="icon-time"></i>
                                          <span class="green">4 sec</span>
                                        </div>
                            
                                        <div class="name">
                                          <a href="#">Alexa</a>
                                        </div>
                                        <div class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
                            
                                        <div class="tools">
                                          <a href="#" class="btn btn-minier btn-info">
                                            <i class="icon-only icon-share-alt"></i>
                                          </a>
                                        </div>
                                      </div>
                                    </div>
                            
                                    <div class="itemdiv dialogdiv">
                                      <div class="user">
                                        <img alt="John's Avatar" src="/assets/avatars/avatar.png" />
                                      </div>
                            
                                      <div class="body">
                                        <div class="time">
                                          <i class="icon-time"></i>
                                          <span class="blue">38 sec</span>
                                        </div>
                            
                                        <div class="name">
                                          <a href="#">John</a>
                                        </div>
                                        <div class="text">Raw denim you probably haven&#39;t heard of them jean shorts Austin.</div>
                            
                                        <div class="tools">
                                          <a href="#" class="btn btn-minier btn-info">
                                            <i class="icon-only icon-share-alt"></i>
                                          </a>
                                        </div>
                                      </div>
                                    </div>
                            
                                    <div class="itemdiv dialogdiv">
                                      <div class="user">
                                        <img alt="Bob's Avatar" src="/assets/avatars/user.jpg" />
                                      </div>
                            
                                      <div class="body">
                                        <div class="time">
                                          <i class="icon-time"></i>
                                          <span class="orange">2 min</span>
                                        </div>
                            
                                        <div class="name">
                                          <a href="#">Bob</a>
                                        </div>
                                        <div class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque commodo massa sed ipsum porttitor facilisis.</div>
                            
                                        <div class="tools">
                                          <a href="#" class="btn btn-minier btn-info">
                                            <i class="icon-only icon-share-alt"></i>
                                          </a>
                                        </div>
                                      </div>
                                    </div>
                            
                                  </div>
                            
                                </div><!-- /widget-main -->
                              </div><!-- /widget-body -->
                            </div><!-- /widget-box -->
                          </div>
                        </div>
                        
                      </form>
                      
                    </div>

                    <div id="faq-tab-2" class="tab-pane fade">


                    </div>

                    <div id="faq-tab-3" class="tab-pane fade">


                    </div>

                    <div id="faq-tab-4" class="tab-pane fade">


                    </div>

                  </div>
                </div>


								<!-- PAGE CONTENT BEGINS -->


  						</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div><!-- /.main-content -->

			</div><!-- /.main-container-inner -->

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="icon-double-angle-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->

		<script type="text/javascript">
			window.jQuery || document.write("<script src='/assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='/assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->

		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="/assets/js/bootstrap.min.js"></script>
		<script src="/assets/js/typeahead-bs2.min.js"></script>

		<!-- page specific plugin scripts -->

		<!--[if lte IE 8]>
		  <script src="/assets/js/excanvas.min.js"></script>
		<![endif]-->

		<script src="/assets/js/jquery-ui-1.10.3.custom.min.js"></script>
		<script src="/assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="/assets/js/chosen.jquery.min.js"></script>
		<script src="/assets/js/fuelux/fuelux.spinner.min.js"></script>
		<script src="/assets/js/date-time/bootstrap-datepicker.min.js"></script>
		<script src="/assets/js/date-time/bootstrap-timepicker.min.js"></script>
		<script src="/assets/js/date-time/moment.min.js"></script>
		<script src="/assets/js/date-time/daterangepicker.min.js"></script>
		<script src="/assets/js/bootstrap-colorpicker.min.js"></script>
		<script src="/assets/js/jquery.knob.min.js"></script>
		<script src="/assets/js/jquery.autosize.min.js"></script>
		<script src="/assets/js/jquery.inputlimiter.1.3.1.min.js"></script>
		<script src="/assets/js/jquery.maskedinput.min.js"></script>
		<script src="/assets/js/bootstrap-tag.min.js"></script>

		<!-- ace scripts -->

		<script src="assets/js/ace-elements.min.js"></script>
		<script src="/assets/js/ace.min.js"></script>

		<!-- inline scripts related to this page -->

		<script type="text/javascript">
			jQuery(function($) {
				$('#id-disable-check').on('click', function() {
					var inp = $('#form-input-readonly').get(0);
					if(inp.hasAttribute('disabled')) {
						inp.setAttribute('readonly' , 'true');
						inp.removeAttribute('disabled');
						inp.value="This text field is readonly!";
					}
					else {
						inp.setAttribute('disabled' , 'disabled');
						inp.removeAttribute('readonly');
						inp.value="This text field is disabled!";
					}
				});
			
			
				$(".chosen-select").chosen(); 
				$('#chosen-multiple-style').on('click', function(e){
					var target = $(e.target).find('input[type=radio]');
					var which = parseInt(target.val());
					if(which == 2) $('#form-field-select-4').addClass('tag-input-style');
					 else $('#form-field-select-4').removeClass('tag-input-style');
				});
			
			
				$('[data-rel=tooltip]').tooltip({container:'body'});
				$('[data-rel=popover]').popover({container:'body'});
				
				$('textarea[class*=autosize]').autosize({append: "\n"});
				$('textarea.limited').inputlimiter({
					remText: '%n character%s remaining...',
					limitText: 'max allowed : %n.'
				});
			
				$.mask.definitions['~']='[+-]';
				$('.input-mask-date').mask('99/99/9999');
				$('.input-mask-phone').mask('(999) 999-9999');
				$('.input-mask-eyescript').mask('~9.99 ~9.99 999');
				$(".input-mask-product").mask("a*-999-a999",{placeholder:" ",completed:function(){alert("You typed the following: "+this.val());}});
			
			
			
				$( "#input-size-slider" ).css('width','200px').slider({
					value:1,
					range: "min",
					min: 1,
					max: 8,
					step: 1,
					slide: function( event, ui ) {
						var sizing = ['', 'input-sm', 'input-lg', 'input-mini', 'input-small', 'input-medium', 'input-large', 'input-xlarge', 'input-xxlarge'];
						var val = parseInt(ui.value);
						$('#form-field-4').attr('class', sizing[val]).val('.'+sizing[val]);
					}
				});
			
				$( "#input-span-slider" ).slider({
					value:1,
					range: "min",
					min: 1,
					max: 12,
					step: 1,
					slide: function( event, ui ) {
						var val = parseInt(ui.value);
						$('#form-field-5').attr('class', 'col-xs-'+val).val('.col-xs-'+val);
					}
				});
				
				
				$( "#slider-range" ).css('height','200px').slider({
					orientation: "vertical",
					range: true,
					min: 0,
					max: 100,
					values: [ 17, 67 ],
					slide: function( event, ui ) {
						var val = ui.values[$(ui.handle).index()-1]+"";
			
						if(! ui.handle.firstChild ) {
							$(ui.handle).append("<div class='tooltip right in' style='display:none;left:16px;top:-6px;'><div class='tooltip-arrow'></div><div class='tooltip-inner'></div></div>");
						}
						$(ui.handle.firstChild).show().children().eq(1).text(val);
					}
				}).find('a').on('blur', function(){
					$(this.firstChild).hide();
				});
				
				$( "#slider-range-max" ).slider({
					range: "max",
					min: 1,
					max: 10,
					value: 2
				});
				
				$( "#eq > span" ).css({width:'90%', 'float':'left', margin:'15px'}).each(function() {
					// read initial values from markup and remove that
					var value = parseInt( $( this ).text(), 10 );
					$( this ).empty().slider({
						value: value,
						range: "min",
						animate: true
						
					});
				});
			
				
				$('#id-input-file-1 , #id-input-file-2').ace_file_input({
					no_file:'No File ...',
					btn_choose:'Choose',
					btn_change:'Change',
					droppable:false,
					onchange:null,
					thumbnail:false //| true | large
					//whitelist:'gif|png|jpg|jpeg'
					//blacklist:'exe|php'
					//onchange:''
					//
				});
				
				$('#id-input-file-3').ace_file_input({
					style:'well',
					btn_choose:'Drop files here or click to choose',
					btn_change:null,
					no_icon:'icon-cloud-upload',
					droppable:true,
					thumbnail:'small'//large | fit
					//,icon_remove:null//set null, to hide remove/reset button
					/**,before_change:function(files, dropped) {
						//Check an example below
						//or examples/file-upload.html
						return true;
					}*/
					/**,before_remove : function() {
						return true;
					}*/
					,
					preview_error : function(filename, error_code) {
						//name of the file that failed
						//error_code values
						//1 = 'FILE_LOAD_FAILED',
						//2 = 'IMAGE_LOAD_FAILED',
						//3 = 'THUMBNAIL_FAILED'
						//alert(error_code);
					}
			
				}).on('change', function(){
					//console.log($(this).data('ace_input_files'));
					//console.log($(this).data('ace_input_method'));
				});
				
			
				//dynamically change allowed formats by changing before_change callback function
				$('#id-file-format').removeAttr('checked').on('change', function() {
					var before_change
					var btn_choose
					var no_icon
					if(this.checked) {
						btn_choose = "Drop images here or click to choose";
						no_icon = "icon-picture";
						before_change = function(files, dropped) {
							var allowed_files = [];
							for(var i = 0 ; i < files.length; i++) {
								var file = files[i];
								if(typeof file === "string") {
									//IE8 and browsers that don't support File Object
									if(! (/\.(jpe?g|png|gif|bmp)$/i).test(file) ) return false;
								}
								else {
									var type = $.trim(file.type);
									if( ( type.length > 0 && ! (/^image\/(jpe?g|png|gif|bmp)$/i).test(type) )
											|| ( type.length == 0 && ! (/\.(jpe?g|png|gif|bmp)$/i).test(file.name) )//for android's default browser which gives an empty string for file.type
										) continue;//not an image so don't keep this file
								}
								
								allowed_files.push(file);
							}
							if(allowed_files.length == 0) return false;
			
							return allowed_files;
						}
					}
					else {
						btn_choose = "Drop files here or click to choose";
						no_icon = "icon-cloud-upload";
						before_change = function(files, dropped) {
							return files;
						}
					}
					var file_input = $('#id-input-file-3');
					file_input.ace_file_input('update_settings', {'before_change':before_change, 'btn_choose': btn_choose, 'no_icon':no_icon})
					file_input.ace_file_input('reset_input');
				});
			
			
			
			
				$('#spinner1').ace_spinner({value:0,min:0,max:200,step:10, btn_up_class:'btn-info' , btn_down_class:'btn-info'})
				.on('change', function(){
					//alert(this.value)
				});
				$('#spinner2').ace_spinner({value:0,min:0,max:10000,step:100, touch_spinner: true, icon_up:'icon-caret-up', icon_down:'icon-caret-down'});
				$('#spinner3').ace_spinner({value:0,min:-100,max:100,step:10, on_sides: true, icon_up:'icon-plus smaller-75', icon_down:'icon-minus smaller-75', btn_up_class:'btn-success' , btn_down_class:'btn-danger'});
			
			
				
				$('.date-picker').datepicker({autoclose:true}).next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
				$('input[name=date-range-picker]').daterangepicker().prev().on(ace.click_event, function(){
					$(this).next().focus();
				});
				
				$('#timepicker1').timepicker({
					minuteStep: 1,
					showSeconds: true,
					showMeridian: false
				}).next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
				
				$('#colorpicker1').colorpicker();
				$('#simple-colorpicker-1').ace_colorpicker();
			
				
				$(".knob").knob();
				
				
				//we could just set the data-provide="tag" of the element inside HTML, but IE8 fails!
				var tag_input = $('#form-field-tags');
				if(! ( /msie\s*(8|7|6)/.test(navigator.userAgent.toLowerCase())) ) 
				{
					tag_input.tag(
					  {
						placeholder:tag_input.attr('placeholder'),
						//enable typeahead by specifying the source array
						source: ace.variable_US_STATES,//defined in ace.js >> ace.enable_search_ahead
					  }
					);
				}
				else {
					//display a textarea for old IE, because it doesn't support this plugin or another one I tried!
					tag_input.after('<textarea id="'+tag_input.attr('id')+'" name="'+tag_input.attr('name')+'" rows="3">'+tag_input.val()+'</textarea>').remove();
					//$('#form-field-tags').autosize({append: "\n"});
				}
				
				
				
			
				/////////
				$('#modal-form input[type=file]').ace_file_input({
					style:'well',
					btn_choose:'Drop files here or click to choose',
					btn_change:null,
					no_icon:'icon-cloud-upload',
					droppable:true,
					thumbnail:'large'
				})
				
				//chosen plugin inside a modal will have a zero width because the select element is originally hidden
				//and its width cannot be determined.
				//so we set the width after modal is show
				$('#modal-form').on('shown.bs.modal', function () {
					$(this).find('.chosen-container').each(function(){
						$(this).find('a:first-child').css('width' , '210px');
						$(this).find('.chosen-drop').css('width' , '210px');
						$(this).find('.chosen-search input').css('width' , '200px');
					});
				})
				/**
				//or you can activate the chosen plugin after modal is shown
				//this way select element becomes visible with dimensions and chosen works as expected
				$('#modal-form').on('shown', function () {
					$(this).find('.modal-chosen').chosen();
				})
				*/
			
			});
		</script>
	</body>
</html>

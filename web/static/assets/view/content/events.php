
            <div class="row">
              <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->

                <div class="row">
                  <div class="col-sm-12">
                    <div class="well well-lg">
                      <h4 class="blue">Vess Regional Conference in Argentina Coming Soon!</h4>
                      Don't forget to register for the Vess Regional Conference in Argentina. Limited seating and lots of new information on the changes to our platform.
                    </div>                  
                  </div>
                </div>

                <div class="row">

                  <div class="col-sm-3">
                    <div class="widget-box transparent">
                      <div class="widget-header">
                        <h4>Upcoming Events</h4>
                      </div>
                    </div>
                    
                    <div id="accordion" class="accordion-style1 panel-group">
                      <div class="panel panel-default">
                        <div class="panel-heading">
                          <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                              <i class="icon-angle-down bigger-110" data-icon-hide="icon-angle-down" data-icon-show="icon-angle-right"></i>
                              &nbsp;Event 1
                            </a>
                          </h4>
                        </div>
                    
                        <div class="panel-collapse collapse in" id="collapseOne">
                          <div class="panel-body">
                            Event 1 Details - Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                          </div>
                        </div>
                      </div>
                    
                      <div class="panel panel-default">
                        <div class="panel-heading">
                          <h4 class="panel-title">
                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                              <i class="icon-angle-right bigger-110" data-icon-hide="icon-angle-down" data-icon-show="icon-angle-right"></i>
                              &nbsp;Event 2
                            </a>
                          </h4>
                        </div>
                    
                        <div class="panel-collapse collapse" id="collapseTwo">
                          <div class="panel-body">
                            Event 2 Details - Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                          </div>
                        </div>
                      </div>
                    
                      <div class="panel panel-default">
                        <div class="panel-heading">
                          <h4 class="panel-title">
                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                              <i class="icon-angle-right bigger-110" data-icon-hide="icon-angle-down" data-icon-show="icon-angle-right"></i>
                              &nbsp;Event 3
                            </a>
                          </h4>
                        </div>
                    
                        <div class="panel-collapse collapse" id="collapseThree">
                          <div class="panel-body">
                            Event 2 Details - Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
                          </div>
                        </div>
                      </div>
                    </div>
                    
                  </div>

                  <div class="col-sm-9">
                    <div class="widget-box transparent">
                      <div class="widget-header">
                        <h4>Calendar of Events</h4>
                      </div>
                    </div>
                    <div class="space"></div>
                    <div id="calendar"></div>
                  </div>


                </div>


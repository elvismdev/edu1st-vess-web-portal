<?php

  include('assets/inc/global_variables.php');
  
  $this_page = "Dashboard";
  $page_title = $this_page." | ".$_9rlabs_company_name;
  
  $breadcrumbs = "<li class=\"active\">".$this_page."</li>";

  include('assets/view/sections/header.php');
  include('assets/view/sections/navbar.php');

?>

    <div class="main-container" id="main-container">

      <div class="main-container-inner">
        <a class="menu-toggler" id="menu-toggler" href="#">
          <span class="menu-text"></span>
        </a>

        <?php include('assets/view/sections/sidebar.php'); ?>  

        <div class="main-content">

          <?php include('assets/view/sections/breadcrumbs.php'); ?> 

          <div class="page-content">

            <div class="space-12"></div>
            <div class="space-12"></div>
            <div class="space-12"></div>
            <div class="space-12"></div>
            <div class="space-12"></div>
            <div class="space-12"></div>

            <div class="page-header">
              <h1 style="color:#1c69a5;">Dashboard</h1>
            </div><!-- /.page-header -->

            <div class="row">
              <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
                
                <?php include('assets/view/content/home.php'); ?> 
                
                <!-- PAGE CONTENT ENDS -->
              </div><!-- /.col -->
            </div><!-- /.row -->
          </div><!-- /.page-content -->
        </div><!-- /.main-content -->

      </div><!-- /.main-container-inner -->

      <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="icon-double-angle-up icon-only bigger-110"></i>
      </a>
    </div><!-- /.main-container -->

    <?php include('assets/inc/javascripts.php'); ?>

  </body>
</html>
<!DOCTYPE html>
<html lang="en">
	<head>

  	<meta charset="utf-8" />
		<meta name="description" content="User login page" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

		<title>Dashboard Login | <?php echo $_9rlabs_company_name; ?></title>

		<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">

		<!-- basic styles -->

		<link rel="stylesheet" href="/assets/css/bootstrap.min.css"  />
		<link rel="stylesheet" href="/assets/css/font-awesome.min.css" />

		<!--[if IE 7]>
		  <link rel="stylesheet" href="/assets/css/font-awesome-ie7.min.css" />
		<![endif]-->

		<!-- page specific plugin styles -->

		<!-- fonts -->

		<link rel="stylesheet" href="/assets/css/ace-fonts.css" />

		<!-- ace styles -->

		<link rel="stylesheet" href="/assets/css/ace.min.css" />
		<link rel="stylesheet" href="/assets/css/ace-rtl.min.css" />

		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="/assets/css/ace-ie.min.css" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

		<!--[if lt IE 9]>
		<script src="/assets/js/html5shiv.js"></script>
		<script src="/assets/js/respond.min.js"></script>
		<![endif]-->

	</head>

	<body class="login-layout" style="background-color:#0b304d;">
		<div class="main-container">
			<div class="main-content">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<div class="login-container">
							<div class="center">
              	<a href="/"><img src="assets/images/VESS-Login-Logo.png" style="padding-top:10px;" /></a>
							</div>

							<div class="space-6"></div>

							<div class="position-relative">
								<div id="login-box" class="login-box visible widget-box no-border" style="background-color:#FFFFFF;">
									<div class="widget-body">
										<div class="widget-main" style="background-color:#f7f7f7">
											<h4 class="header blue lighter bigger">
												<i class="icon-home blue"></i>
												Dashboard Login
											</h4>

											<div class="space-6"></div>
											<form name='9r_form_login' id='9r_form_login'>
												<fieldset>
		                      <div id="login_error_email" style="color:#b94a48;"></div>
													<label class="block clearfix">
														<span id="_9r_form_login_email_span" class="block input-icon input-icon-right">
															<input data-rel="tooltip" type="text" name="_9r_form_login_email" id="_9r_form_login_email" class="form-control" placeholder="Email" autocomplete="off" onKeyDown="$('#_9r_form_login_email_span').removeClass('has-error'); $('#login_error_email').html(''); $('#login_error_returned').html('');" title="Your Email Address" data-placement="bottom" />
															<i class="icon-user"></i>
														</span>
													</label>

		                      <div id="login_error_password" style="color:#b94a48;"></div>
													<label class="block clearfix">
														<span id="_9r_form_login_password_span" class="block input-icon input-icon-right">
															<input type="password" name="_9r_form_login_password" id="_9r_form_login_password" class="form-control" placeholder="Password" autocomplete="off" onKeyDown="$('#_9r_form_login_password_span').removeClass('has-error'); $('#login_error_password').html(''); $('#login_error_returned').html('');" />
															<i class="icon-lock"></i>
														</span>
													</label>

                          <div id="login_error_returned" style="color:#b94a48; text-align:center;"></div>
													<div class="space"></div>

													<div class="clearfix">
                          
														<label class="inline">
															<input value="remember-me" id="_9r_login_remember_me" type="checkbox" class="ace" />
															<span class="lbl"> Remember Me</span>
														</label>

														<span id="login_button_area">
                              <button id="_9r_form_login_button" type="button" onClick="_9r_form_login();" class="width-35 pull-right btn btn-sm btn-primary">
                                <i class="icon-key"></i>
                                Login
                              </button>
                              <span id="_9r_form_login_button_process" class="lbl pull-right" style="display:none;">Processing Login...</span>
                            </span>
                            
													</div>

													<div class="space-4"></div>
												</fieldset>
											</form>

										</div><!-- /widget-main -->

										<div class="toolbar clearfix" style="border-top:2px solid rgb(255,255,255)">
											<div>
												<a href="<?php echo $_9rlabs_company_url; ?>" class="forgot-password-link" style="color:#FFFFFF">
													<i class="icon-arrow-left" style="color:#FFFFFF"></i>
													Back to Edu1st
												</a>
											</div>

											<div>
												<a href="#" onclick="show_box('forgot-box'); return false;" class="user-signup-link" style="color:#FFFFFF">
													Forgot Password
													<i class="icon-arrow-right" style="color:#FFFFFF"></i>
												</a>
											</div>
										</div>
									</div><!-- /widget-body -->
								</div><!-- /login-box -->

								<div id="forgot-box" class="forgot-box widget-box no-border" style="background-color:#FFFFFF;">
									<div class="widget-body">
										<div class="widget-main" style="background-color:#f7f7f7">
											<h4 class="header blue lighter bigger">
												<i class="icon-key"></i>
												Retrieve Password
											</h4>

											<div class="space-6"></div>
											<p>
												Enter your Email to receive instructions
											</p>

											<form>
												<fieldset>
													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="email" class="form-control" placeholder="Email" />
															<i class="icon-envelope"></i>
														</span>
													</label>

													<div class="clearfix">
														<button type="button" class="width-35 pull-right btn btn-sm btn-primary">
															<i class="icon-lightbulb"></i>
															Send Me!
														</button>
													</div>
												</fieldset>
											</form>
										</div><!-- /widget-main -->

										<div class="toolbar clearfix" style="border-top:2px solid rgb(255,255,255); background-color:#478fca;">
											<a href="#" onclick="show_box('login-box'); return false;" class="back-to-login-link" style="color:#FFFFFF;">
                        <i class="icon-arrow-left" style="color:#FFFFFF;"></i>
  											Back to Login
											</a>
										</div>
									</div><!-- /widget-body -->
								</div><!-- /forgot-box -->

							</div><!-- /position-relative -->
						</div>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div>
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<script type="text/javascript">
			jQuery(function($) {
				$('[data-rel=tooltip]').tooltip({container:'body'});

			});
		</script>
		

		<!--[if !IE]> -->
		<script type="text/javascript">
			window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
		</script>
		<!-- <![endif]-->

		<!--[if IE]>
    <script type="text/javascript">
			window.jQuery || document.write("<script src='assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
    </script>
    <![endif]-->

		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>

		<!-- inline scripts related to this page -->
		<script src="/assets/js/views/login.js"></script>

	</body>
</html>

<?php

  $current_page = $_SERVER['PHP_SELF'];

  $act_dashboard = "";
  $act_modules = "";
  $act_planners = "";
  $act_resources = "";
  $act_products_and_services = "";
  $act_events = "";
  $act_school_network = "";
  $act_support_and_coaching = "";

  $icon_dashboard = "7eba15"; /* WAS e4a235 EP */
  $icon_modules = "e4a235"; /* WAS e4a235 EP */
  $icon_planners = "ed764e"; /* WAS e4a235 EP */
  $icon_resources = "eb1319"; /* WAS e4a235 EP */
  $icon_products_and_services = "7d68c1"; /* WAS e4a235 EP */
  $icon_events = "4195cf"; /* WAS e4a235 EP */
  $icon_school_network = "4fbda4"; /* WAS e4a235 EP */
  $icon_support_and_coaching = "d8a56f"; /* WAS e4a235 EP */

  $link_dashboard = "FFFFFF";
  $link_modules = "FFFFFF";
  $link_planners = "FFFFFF";
  $link_resources = "FFFFFF";
  $link_products_and_services = "FFFFFF";
  $link_events = "FFFFFF";
  $link_school_network = "FFFFFF";
  $link_support_and_coaching = "FFFFFF";

  if ( $current_page == "/Modules/index.php" ) { 
    $link_modules = "1c69a5";
    $icon_modules = "e4a235";
    $act_modules = "active";
    
    if ( $_GET['md'] == "1" ) { $act_modules_1 = "active"; }
    if ( $_GET['md'] == "2" ) { $act_modules_2 = "active"; }
    if ( $_GET['md'] == "3" ) { $act_modules_3 = "active"; }    
    
  } else if ( $current_page == "/Planners/index.php" ) { 
    $link_planners = "1c69a5";
    $icon_planners = "ed764e";
    $act_planners = "active";
  } else if ( $current_page == "/Resources/index.php" ) { 
    $link_resources = "1c69a5";
    $icon_resources = "eb1319";
    $act_resources = "active";
  } else if ( $current_page == "/Products-and-Services/index.php" ) { 
    $link_products_and_services = "1c69a5";
    $icon_products_and_services = "7d68c1";
    $act_products_and_services = "active";
  } else if ( $current_page == "/Events/index.php" ) { 
    $link_events = "1c69a5";
    $icon_events = "4195cf";
    $act_events = "active";
  } else if ( $current_page == "/School-Network/index.php" ) { 
    $link_school_network = "1c69a5";
    $icon_school_network = "4fbda4";
    $act_school_network = "active";
  } else if ( $current_page == "/Support-and-Coaching/index.php" ) { 
    $link_support_and_coaching = "1c69a5";
    $icon_support_and_coaching = "d8a56f";
    $act_support_and_coaching = "active open";
    
    if ( $_GET['md'] == "1" ) { $act_support_and_coaching_1 = "active"; }
    if ( $_GET['md'] == "2" ) { $act_support_and_coaching_2 = "active"; }
    if ( $_GET['md'] == "3" ) { $act_support_and_coaching_3 = "active"; }      
    
  } else {
    $link_dashboard = "1c69a5";
    $icon_dashboard = "7eba15";
    $act_dashboard = "active";
  }



?>


				<div class="sidebar sidebar-fixed" id="sidebar">

					<div class="sidebar-shortcuts" id="sidebar-shortcuts" style="display:none;">
						<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
							<button class="btn btn-success">
								<i class="icon-signal"></i>
							</button>

							<button class="btn btn-info">
								<i class="icon-pencil"></i>
							</button>

							<button class="btn btn-warning">
								<i class="icon-group"></i>
							</button>

							<button class="btn btn-danger">
								<i class="icon-cogs"></i>
							</button>
						</div>

						<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
							<span class="btn btn-success"></span>

							<span class="btn btn-info"></span>

							<span class="btn btn-warning"></span>

							<span class="btn btn-danger"></span>
						</div>
					</div><!-- #sidebar-shortcuts -->

					<ul class="nav nav-list">
          
						<li class="<?php echo $act_dashboard; ?>">
							<a href="/">
								<i class="icon-dashboard" style="color:#<?php echo $icon_dashboard; ?>;"></i>
								<span class="menu-text"> Dashboard </span>
							</a>
						</li>

						<li class="<?php echo $act_modules; ?>">
							<a href="/Modules/?md=1">
								<i class="icon-bar-chart" style="color:#<?php echo $icon_modules; ?>;"></i>
								<span class="menu-text"> Curriculum </span>
							</a>
						</li>

<!--
						<li class="<?php echo $act_modules; ?>">
							<a href="#" class="dropdown-toggle">
								<i class="icon-bar-chart"></i>
								<span class="menu-text"> Curriculum </span>

								<b class="arrow icon-angle-down"></b>
							</a>

              <ul class="submenu">

                <li class="<?php echo $act_modules_1; ?>">
                  <a href="/Modules/?md=1">
                    <i class="icon-double-angle-right"></i>
                    Philosophy
                  </a>
                </li>

                <li class="<?php echo $act_modules_2; ?>">
                  <a href="/Modules/?md=2">
                    <i class="icon-double-angle-right"></i>
                    Themes
                  </a>
                </li>
                
                <li class="<?php echo $act_modules_3; ?>">
                  <a href="/Modules/?md=3">
                    <i class="icon-double-angle-right"></i>
                    Art Focus
                  </a>
                </li>   
                
                <li class="<?php echo $act_modules_3; ?>">
                  <a href="/Modules/?md=3">
                    <i class="icon-double-angle-right"></i>
                    Countries
                  </a>
                </li>                   
              </ul>

						</li>-->
            
						<li class="<?php echo $act_planners; ?>">
							<a href="/Planners/">
								<i class="icon-calendar" style="color:#<?php echo $icon_planners; ?>;"></i>
								<span class="menu-text" > Planners </span>
							</a>
						</li>  

						<li class="<?php echo $act_resources; ?>">
							<a href="/Resources/">
								<i class="icon-briefcase" style="color:#<?php echo $icon_resources; ?>;"></i>
								<span class="menu-text" > Resources </span>
							</a>
						</li> 

						<li class="<?php echo $act_products_and_services; ?>">
							<a href="/Products-and-Services/">
								<i class="icon-check" style="color:#<?php echo $icon_products_and_services; ?>;"></i>
								<span class="menu-text"> Products & Services </span>
							</a>
						</li>    
            
						<li class="<?php echo $act_events; ?>">
							<a href="/Events/">
								<i class="icon-bullhorn" style="color:#<?php echo $icon_events; ?>;"></i>
								<span class="menu-text"> Events </span>
							</a>
						</li>               
            
						<li class="<?php echo $act_school_network; ?>">
							<a href="/School-Network/">
								<i class="icon-group" style="color:#<?php echo $icon_school_network; ?>;"></i>
								<span class="menu-text"> School Network </span>
							</a>
						</li>                        
	
						<li class="<?php echo $act_support_and_coaching; ?>">
							<a href="#" class="dropdown-toggle">
								<i class="icon-exclamation-sign" style="color:#<?php echo $icon_support_and_coaching; ?>;"></i>
								<span class="menu-text"> Support & Coaching </span>

								<b class="arrow icon-angle-down"></b>
							</a>

              <ul class="submenu">
                <li class="<?php echo $act_support_and_coaching_1; ?>">
                  <a href="/Support-and-Coaching/?md=1">
                    <i class="icon-double-angle-right"></i>
                    FAQ
                  </a>
                </li>
                
                <li class="<?php echo $act_support_and_coaching_2; ?>">
                  <a href="/Support-and-Coaching/?md=2">
                    <i class="icon-double-angle-right"></i>
                    Tutorials
                  </a>
                </li>   
                
                <li class="<?php echo $act_support_and_coaching_3; ?>">
                  <a href="/Support-and-Coaching/?md=3">
                    <i class="icon-double-angle-right"></i>
                    Contact Support
                  </a>
                </li>                                
                
              </ul>

						</li>
              
  
					</ul><!-- /.nav-list -->

					<div class="sidebar-collapse" id="sidebar-collapse">
						<i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
					</div>

					<script type="text/javascript">
						try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
					</script>
				</div>
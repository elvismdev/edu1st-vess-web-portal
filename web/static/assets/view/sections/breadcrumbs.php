          <div class="breadcrumbs breadcrumbs-fixed" id="breadcrumbs">

            <ul class="breadcrumb">
              <li>
                <i class="icon-home home-icon" style="color:#979b9e;"></i>
                <a href="/">Home</a>
              </li>
              <?php echo $breadcrumbs; ?>
            </ul><!-- .breadcrumb -->

            <div class="nav-search" id="nav-search" style="display:none;">
              <form class="form-search">
                <span class="input-icon">
                  <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                  <i class="icon-search nav-search-icon"></i>
                </span>
              </form>
            </div><!-- #nav-search -->
          </div>
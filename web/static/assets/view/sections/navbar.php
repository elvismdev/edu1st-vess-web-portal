    <div class="navbar navbar-default navbar-fixed-top" id="navbar">

      <div class="navbar-container" id="navbar-container">
        <div class="navbar-header pull-left">
          <a href="/" class="navbar-brand">
            <img src="/assets/images/VESS-Logo-Dashboard4.png" />
          </a><!-- /.brand -->
        </div><!-- /.navbar-header -->

        <div class="navbar-header pull-right" role="navigation">
          <ul class="nav ace-nav">

            <li class="light-blue">
              <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                <img class="nav-user-photo" src="<?php echo $_9r_user_avatar; ?>" alt="<?php echo $_9r_user_name; ?>'s Photo" />
                <span class="user-info">
                  <small>Welcome,</small>
                  <?php echo $_9r_user_name; ?>
                </span>

                <i class="icon-caret-down"></i>
              </a>

              <ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                <li>
                  <a href="#">
                    <i class="icon-cog" style="color:#1c69a5;"></i>
                    Settings
                  </a>
                </li>

                <li>
                  <a href="#">
                    <i class="icon-user" style="color:#1c69a5;"></i>
                    Profile
                  </a>
                </li>

                <li class="divider"></li>

                <li>
                  <a href="/assets/inc/logout.php">
                    <i class="icon-off" style="color:#1c69a5;"></i>
                    Logout
                  </a>
                </li>
              </ul>
            </li>
          </ul><!-- /.ace-nav -->
        </div><!-- /.navbar-header -->
      </div><!-- /.container -->
    </div>

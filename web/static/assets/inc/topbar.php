		<div class="navbar navbar-default navbar-fixed-top" id="navbar">
			<div class="navbar-container" id="navbar-container">
				<div class="navbar-header pull-left">
					<a href="<?php echo $sub_folder; ?>" class="navbar-brand" style="padding-left:4px;">
						<img src="<?php echo $sub_folder; ?>assets/images/VESS-Logo-Dashboard.png" class="img-responsive" />
					</a><!-- /.brand -->
				</div><!-- /.navbar-header -->

				<div class="navbar-header pull-right" role="navigation">
					<ul class="nav ace-nav">

						<li class="light-blue">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								<img class="nav-user-photo" src="<?php echo $sub_folder; ?>assets/avatars/<?php echo $current_user_avatar; ?>" alt="<?php $current_user; ?>'s Photo" />
								<span class="user-info">
									<small>Welcome,</small>
									<?php echo $current_user; ?>
								</span>

								<i class="icon-caret-down"></i>
							</a>

							<ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
								<li>
									<a href="<?php echo $sub_folder; ?>Profile/">
										<i class="icon-user"></i>
										Profile
									</a>
								</li>

								<li class="divider"></li>

								<li>
									<a href="<?php echo $sub_folder; ?>Logout/">
										<i class="icon-off"></i>
										Logout
									</a>
								</li>
							</ul>
						</li>
					</ul><!-- /.ace-nav -->
				</div><!-- /.navbar-header -->
			</div><!-- /.container -->
		</div>

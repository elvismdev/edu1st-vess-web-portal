<?php

	session_start();
	session_unset();
	session_destroy();
	
	$destination_url = "/";
	header( "Location: $destination_url" );
	die;				

?>
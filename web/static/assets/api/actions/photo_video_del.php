<?php

	// DISABLE NON INCLUDE ACCESS ====================================================================
	if ( $include_check != "2f6d8d787d9598708d96daa6140860040efb33d9" ) {
		$destination_url = "http://www.eliteolympian.com/";
		header( "Location: $destination_url" );
		die;	
	}
	// ===============================================================================================	
	
	// GET VARIABLES FROM FORM POST ==================================================================
	if ( $api_test == true ) {
		$file_id = $_GET['file_id'];
		$delete_option = $_GET['delete_option'];
	} else {
		$file_id = $_POST['file_id'];
		$delete_option = $_POST['delete_option'];	
	}
	// ===============================================================================================	
	
	// CANCEL ACTION IF REQUIRED VARIABLES NOT PRESENT ===============================================
	if (( $file_id == "" ) || ( $delete_option == "" )) {
		$output_error = json_error($action, 'Missing Variables');
		echo $output_error;
		die;
	}
	// ===============================================================================================

	// CHECK FOR OPTION ==============================================================================
	if (( $delete_option != 'del' ) && ( $delete_option != 'rec' )) {
		$output_error = json_error($action, 'Invalid Option');
		echo $output_error;
		die;
	}
	// ===============================================================================================

	// RENAME FILE DETAILS ON SERVER =================================================================
	database_connect('app');

	switch ($update_option) {
		case "del":
			$query_mysql = "UPDATE user_media SET deleted='Y' WHERE id='$file_id'";
			$del_action = "deleted";
			break;
		case "rec":
			$query_mysql = "UPDATE user_media SET deleted='' WHERE id='$file_id'";
			$del_action = "recovered";
			break;
	}
	
	$result_mysql = mysql_query($query_mysql);
	
	echo "{\"ok\":true,\"code\":\"file-deleted\",\"action\":\"".$del_action."\"}";

	database_close();	
	// ===============================================================================================

?>

var recent_vid = Titanium.Media.createVideoPlayer({
            media: event.media  //this object is from video recording's success event
        });
                // get the frame image from the video
        var thumbnail = recent_vid.thumbnailImageAtTime (2, Titanium.Media.VIDEO_TIME_OPTION_NEAREST_KEYFRAME);
        var thumb2 = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory,"thumbnail2.jpg");
 
               // create the scaled down image view
        var sImg = Titanium.UI.createImageView({
            image: thumbnail,
            width: 24,
            height: 32
        });
 
                // now save the rendered image to the file
        thumb2.write(sImg.toImage());
        SilentCam.thumbnail += "Done write thumbnail";
<?php

	// DISABLE NON INCLUDE ACCESS ====================================================================
	if ( $include_check != "2f6d8d787d9598708d96daa6140860040efb33d9" ) {
		$destination_url = "http://www.eliteolympian.com/";
		header( "Location: $destination_url" );
		die;	
	}
	// ===============================================================================================	

	// GET VARIABLES FROM FORM POST ==================================================================
	if ( $api_test == true ) {
		$name_f = $_GET['name_f'];
		$name_l = $_GET['name_l'];
		$email = $_GET['email'];
		$login = $_GET['login'];
		$pass = $_GET['pass'];
	} else {
		$name_f = $_POST['name_f'];
		$name_l = $_POST['name_l'];
		$email = $_POST['email'];
		$login = $_POST['login'];
		$pass = $_POST['pass'];
	}
	// ===============================================================================================	
	
	// CANCEL ACTION IF REQUIRED VARIABLES NOT PRESENT ===============================================
	if ( ( $login == "" ) || ( $pass == "" ) || ( $email == "" ) || ( $name_f == "" ) || ( $name_l == "" ) ) {
		$output_error = json_error($action, 'Missing Variables');
		echo $output_error;
		die;
	}
	// ===============================================================================================	

	// LOGIN PROCESS AND VALIDATION ==================================================================
	database_connect('amember');

	$email_exists = false;
	$login_exists = false;

	// Check Existing Email
	$query_mysql = "SELECT user_id, email FROM am_user WHERE email='$email'";
	$result_mysql = mysql_query($query_mysql);	
	if ( mysql_num_rows($result_mysql) != 0 ) { $email_exists = true; }
	
	// Check Existing Login
	$query_mysql = "SELECT user_id, login FROM am_user WHERE login='$login'";
	$result_mysql = mysql_query($query_mysql);	
	if ( mysql_num_rows($result_mysql) != 0 ) { $login_exists = true; }	

	if (( $email_exists == true ) || ( $login_exists == true )) {
		
		if (( $email_exists == true ) && ( $login_exists == true )) { 
			$exists_message = "Email and Username already exists";
		} else if ( ( $email_exists == true ) && ( $login_exists == false ) ) {
			$exists_message = "Email already exists";			
		} else if ( ( $email_exists == false ) && ( $login_exists == true ) ) {
			$exists_message = "Username already exists";			
		}		
		$output_error = json_error($action, $exists_message);
		echo $output_error;
		die;
				
	} else {
	
		$url = "https://www.eliteolympian.com/amember/api/users";	
		$data = "_key=".$amember_api_key."&login=".$login."&pass=".$pass."&email=".$email."&name_f=".$name_f."&name_l=".$name_l."&status=1&_format=json";
		$create_user = DoPostRequest($url, $data);
		echo $create_user;
		
	}

	database_close();		
	// ===============================================================================================	

	die;

?>
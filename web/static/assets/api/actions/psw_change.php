<?php

	// DISABLE NON INCLUDE ACCESS ====================================================================
	if ( $include_check != "2f6d8d787d9598708d96daa6140860040efb33d9" ) {
		$destination_url = "http://www.eliteolympian.com/";
		header( "Location: $destination_url" );
		die;	
	}
	// ===============================================================================================	

	// GET VARIABLES FROM FORM POST ==================================================================
	if ( $api_test == false ) {
		$old_pass = $_GET['old_pass'];
		$new_pass = $_GET['new_pass'];
	} else {
		$old_pass = $_POST['old_pass'];
		$new_pass = $_POST['new_pass'];
	}
	// ===============================================================================================	

	// CANCEL ACTION IF REQUIRED VARIABLES NOT PRESENT ===============================================
	if ( ( $member_id == "" ) || ( $old_pass == "" ) || ( $new_pass == "" ) ) {
		$output_error = json_error($action, 'Missing Variables');
		echo $output_error;
		die;
	}
	// ===============================================================================================	

	// INCLUDE ENCRYPTION FUNCTIONALITY ==============================================================
	include($_SERVER['DOCUMENT_ROOT']."/app/includes/encdec.php");
	// ===============================================================================================	

	session_start();
	session_regenerate_id();

	// LOGIN PROCESS AND VALIDATION ==================================================================
	database_connect('amember');

	$query_mysql = "SELECT user_id, pass_enc FROM am_user WHERE user_id='$member_id'";
	$result_mysql = mysql_query($query_mysql);
	if ( mysql_num_rows($result_mysql) == 0 ) { 
		$output_error = json_error($action, "User ID #".$member_id." is not valid");
		echo $output_error; 
	} else {
		while ( $myrow = mysql_fetch_array($result_mysql)) {
			$pass_enc = $myrow['pass_enc'];
			if ( $pass_enc != enc($old_pass, $pvk1, $iv1) ) {
				echo '{"ok":false,"code":-2,"msg":"Current Password is incorrect"}';
			} else {
				if ( $pass_enc == enc($new_pass, $pvk1, $iv1) ) {
					echo '{"ok":false,"code":-2,"msg":"New Password cannot be the same as old password"}';
				} else {
					$url = "https://www.eliteolympian.com/amember/api/users/".$member_id."?_key=".$amember_api_key."&_method=PUT&pass=".$new_pass;
					$psw_url = file_get_contents($url);
					echo $psw_url;
				}
			}
		}
	}

	database_close();		
	// ===============================================================================================	

	// ACCOUNT CREATION LOGIN TO USER AND PRICIPALS ==================================================
	// USING AMAZON S3 ===============================================================================

	die;

?>
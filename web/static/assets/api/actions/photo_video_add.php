<?php

	// DISABLE NON INCLUDE ACCESS ====================================================================
	if ( $include_check != "2f6d8d787d9598708d96daa6140860040efb33d9" ) {
		$destination_url = "http://www.eliteolympian.com/";
		header( "Location: $destination_url" );
		die;	
	}
	// ===============================================================================================	

	// GET VARIABLES FROM FORM POST ==================================================================
	/*
	if ( $api_test == true ) {
	} else {
	}
	*/
	// ===============================================================================================	

	// CANCEL ACTION IF REQUIRED VARIABLES NOT PRESENT ===============================================
	/*	
	if ( ( $box_id == "" ) || ( $cal_last_update_date == "" ) || ( $cal_last_update_time == "" ) ) {
		$output_error = json_error($action, 'Missing Variables');
		echo $output_error;
		die;
	}
	*/
	// ===============================================================================================	

	// DEFINE ALLOW FILE TYPES =======================================================================
	$allowedExts = array("mov", "mp4", "jpg");
	// ===============================================================================================	

	// GET UPLOADED FILE TYPE ========================================================================
	$temp = explode(".", $_FILES["file"]["name"]);
	$extension = strtolower(end($temp));
	// ===============================================================================================	

	// CHECK FOR FILE TYPE AND THEN UPLOAD ===========================================================
	if ( ( ($_FILES["file"]["type"] == "image/jpeg") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "video/mp4") || ($_FILES["file"]["type"] == "video/quicktime") || ($_FILES["file"]["type"] == "video/mov") ) && in_array($extension, $allowedExts)) {
	  if ($_FILES["file"]["error"] > 0) {
			$output_error = json_error($action, "File Upload Error => ".$_FILES['file']['error']."");
			echo $output_error; 			
    } else {
			
			// ADD ENTRY INTO DATABASE ===================================================================
			database_connect('app');
			
			$query_mysql = "INSERT INTO user_media (member_id, file_name, file_type, file_size, extension ) VALUES ('$member_id', '".$_FILES["file"]["name"]."', '".$_FILES["file"]["type"]."', '".$_FILES["file"]["size"]."', '".$extension."')";
			$result_mysql = mysql_query($query_mysql);
			
			$file_id = mysql_insert_id();
			
			database_close();	
			// ===========================================================================================
			
			// CREATE USER SPECIFIC DIRECTORY ============================================================	
			if(!is_dir("../user_media/".$member_id)) mkdir("../user_media/".$member_id);
			// ===========================================================================================

			// MOVE FILE AND RENAME ======================================================================
			$new_file_name = $member_id."_".$file_id.".".$extension;
      move_uploaded_file($_FILES["file"]["tmp_name"], "../user_media/".$member_id."/". $_FILES["file"]["name"]);
			rename("../user_media/".$member_id."/". $_FILES["file"]["name"], "../user_media/".$member_id."/".$new_file_name);
			// ===========================================================================================
				
			// OUTPUT FILE NAME FROM SERVER ==============================================================
			echo "{\"ok\":true,\"code\":\"upload-success\",\"filename\":\"".$new_file_name."\"}";
			// ===========================================================================================
	
    }
  } else {
//		$output_error = json_error($action, "Invalid File Format => ".$_FILES['file']['name']."");
		echo "FileTempName => \"".$_FILES["file"]["tmp_name"]."\" || FileName => \"".$_FILES["file"]["name"]."\" || FileType => \"".$_FILES["file"]["type"]."\" || FileSize => \"".$_FILES["file"]["size"]."\" || FileExt => \"".$extension."\"";
//		echo $output_error; 			
  }
	
?>
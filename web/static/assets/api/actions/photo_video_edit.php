<?php

	// DISABLE NON INCLUDE ACCESS ====================================================================
	if ( $include_check != "2f6d8d787d9598708d96daa6140860040efb33d9" ) {
		$destination_url = "http://www.eliteolympian.com/";
		header( "Location: $destination_url" );
		die;	
	}
	// ===============================================================================================	
	
	// GET VARIABLES FROM FORM POST ==================================================================
	if ( $api_test == true ) {
		$file_id = $_GET['file_id'];
		$update_option = $_GET['update_option'];
		$update_content = addslashes($_GET['update_content']);
	} else {
		$file_id = $_POST['file_id'];
		$update_option = $_POST['update_option'];	
		$update_content = addslashes($_POST['update_content']);				
	}
	// ===============================================================================================	
	
	// CANCEL ACTION IF REQUIRED VARIABLES NOT PRESENT ===============================================
	if (( $file_id == "" ) || ( $update_option == "" )) {
		$output_error = json_error($action, 'Missing Variables');
		echo $output_error;
		die;
	}
	// ===============================================================================================

	// CHECK FOR OPTION ==============================================================================
	if (( $update_option != 'title' ) && ( $update_option != 'desc' )) {
		$output_error = json_error($action, 'Invalid Option');
		echo $output_error;
		die;
	}
	// ===============================================================================================

	// RENAME FILE DETAILS ON SERVER =================================================================
	database_connect('app');

	switch ($update_option) {
		case "title":
			$query_mysql = "UPDATE user_media SET title='$update_content' WHERE id='$file_id'";
			break;
		case "desc":
			$query_mysql = "UPDATE user_media SET description='$update_content' WHERE id='$file_id'";
			break;
	}
	
	$result_mysql = mysql_query($query_mysql);
	
	echo "{\"ok\":true,\"code\":\"file-edit\",\"".$update_option."\":\"updated\"}";

	database_close();	
	// ===============================================================================================

?>
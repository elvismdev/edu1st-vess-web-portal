<?php

	// DISABLE NON INCLUDE ACCESS ====================================================================
	if ( $include_check != "2f6d8d787d9598708d96daa6140860040efb33d9" ) {
		$destination_url = "http://www.eliteolympian.com/";
		header( "Location: $destination_url" );
		die;	
	}
	// ===============================================================================================	

	// GET VARIABLES FROM FORM POST ==================================================================
	if ( $api_test == true ) {
		$box_id = $_GET['box_id'];
		$cal_last_update_date = $_GET['cal_last_update_date'];
		$cal_last_update_time = $_GET['cal_last_update_time'];
	} else {
		$box_id = $_POST['box_id'];
		$cal_last_update_date = $_POST['cal_last_update_date'];	
		$cal_last_update_time = $_POST['cal_last_update_time'];				
	}
	// ===============================================================================================	

	// CANCEL ACTION IF REQUIRED VARIABLES NOT PRESENT ===============================================
	if ( ( $box_id == "" ) || ( $cal_last_update_date == "" ) || ( $cal_last_update_time == "" ) ) {
		$output_error = json_error($action, 'Missing Variables');
		echo $output_error;
		die;
	}
	// ===============================================================================================	

	// CONVERT DATE ==================================================================================
	$cal_last_update = date('Y-m-d H:i:s', strtotime($cal_last_update_date." ".$cal_last_update_time));
	// ===============================================================================================	

	// LOGIN PROCESS AND VALIDATION ==================================================================
	database_connect('app');

	$query_mysql = "SELECT `WOD_calendar`.`event_id`, `WOD_calendar`.`wod_id`, `WOD_calendar`.`wod_date`, `WOD`.`title`, `WOD`.`description`, `WOD`.`video`, `WOD_calendar`.`time_beg`, `WOD_calendar`.`time_end`, `WOD_calendar`.`slots`, `WOD_calendar`.`status`, `WOD_calendar`.`event_type` FROM `WOD_calendar` INNER JOIN `WOD` ON `WOD_calendar`.`wod_id`=`WOD`.`wod_id` WHERE `WOD_calendar`.`box_id`='$box_id' AND ( `WOD_calendar`.`last_update` > '$cal_last_update' OR `WOD_calendar`.`last_update` = '0000-00-00 00:00:00') ORDER BY wod_date ASC";
	$result_mysql = mysql_query($query_mysql);
	if ( mysql_num_rows($result_mysql) == 0 ) { 
		$output_error = json_error($action, "No Updates");
		echo '[{"msg":"No Updates"}]'; 
	} else {
		while ( $myrow = mysql_fetch_array($result_mysql)) {
			$event_id = $myrow['event_id'];
			$wod_id = $myrow['wod_id'];
			$wod_date = $myrow['wod_date'];
			$title = $myrow['title'];
			$description = $myrow['description'];
			$video = $myrow['video'];
			$time_beg = $myrow['time_beg'];
			$time_end = $myrow['time_end'];
			$slots = $myrow['slots'];
			$status = $myrow['status'];
			$event_type = $myrow['event_type'];

			$cal_event .= '{ "event_id":"'.$event_id.'", "wod_id":"'.$wod_id.'", "wod_date":"'.$wod_date.'", "title":"'.addslashes($title).'", "description":"'.addslashes($description).'", "video":"'.$video.'", "time_beg":"'.$time_beg.'", "time_end":"'.$time_end.'", "slots":"'.$slots.'", "status":"'.$status.'", "type":"'.$event_type.'"},';
			
		}
		
		echo "[".substr_replace($cal_event ,"",-1)."]";
// 		echo "{\"timestamp\":\"".date("m/d/Y-H:i:s", time())."\", \"events\":[".substr_replace($cal_event ,"",-1)."]}";
		
	}

	database_close();		
	// ===============================================================================================	

	die;

?>
<?php

	// DISABLE NON INCLUDE ACCESS ====================================================================
	if ( $include_check != "2f6d8d787d9598708d96daa6140860040efb33d9" ) {
		$destination_url = "http://www.eliteolympian.com/";
		header( "Location: $destination_url" );
		die;	
	}
	// ===============================================================================================	

	// GET VARIABLES FROM FORM POST ==================================================================
	if ( $api_test == true ) {
		$login = $_GET['login'];
		$pass = $_GET['pass'];
	} else {
		$login = $_POST['login'];
		$pass = $_POST['pass'];
	}
	// ===============================================================================================	
	
	// CANCEL ACTION IF REQUIRED VARIABLES NOT PRESENT ===============================================
	if ( ( $login == "" ) || ( $pass == "" ) ) {
		$output_error = json_error($action, 'Missing Variables');
		echo $output_error;
		die;
	}
	// ===============================================================================================	

	// INCLUDE ENCRYPTION FUNCTIONALITY ==============================================================
	include($_SERVER['DOCUMENT_ROOT']."/app/includes/encdec.php");
	// ===============================================================================================	

	// LOGIN PROCESS AND VALIDATION ==================================================================
	database_connect('amember');
	$query_mysql = "SELECT user_id, email, name_f, name_l, pass_enc FROM am_user WHERE login='$login'";
	$result_mysql = mysql_query($query_mysql);
	if (mysql_num_rows($result_mysql) == 0) {
		$output_error = json_error($action, "Username doesn't exist");
		echo $output_error;
	} else {
		while ( $myrow = mysql_fetch_array($result_mysql)) {
			$pass_enc = $myrow['pass_enc'];
			if ( $pass_enc != enc($pass, $pvk1, $iv1) ) {
				echo '{"ok":false,"code":-2,"msg":"The user name or password is incorrect"}';
			} else {
				$user_id = "\"user_id\":".$myrow['user_id'];
				$email = "\"email\":\"".$myrow['email']."\"";
				$name_f = "\"name_f\":\"".$myrow['name_f']."\"";
				$name_l = "\"name_l\":\"".$myrow['name_l']."\"";
				echo "{\"ok\":true,\"code\":\"login-success\",".$user_id.",".$email.",".$name_f.",".$name_l."}";
			}
		}
	}
	database_close();		
	// ===============================================================================================	

	// ACCOUNT CREATION LOGIN TO USER AND PRICIPALS ==================================================
	// USING AMAZON S3 ===============================================================================

	die;

?>
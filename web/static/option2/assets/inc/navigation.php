<?php

  if ( $this_page == "Home" ) { $act_dashboard = "active"; }
  elseif ( $this_page == "Curriculum" ) { $act_curriculum = "active"; }
  elseif ( $this_page == "Planners" ) { $act_planners = "active"; }
  elseif ( $this_page == "Resources" ) { $act_resources = "active"; }
  elseif ( $this_page == "Products & Services" ) { $act_products_and_services = "active"; }
  elseif ( $this_page == "Events" ) { $act_events = "active"; }
  elseif ( $this_page == "School Network" ) { $act_school_network = "active"; }
  elseif ( $this_page == "Support & Coaching" ) { $act_school_support = "active"; }

?>

				<div class="sidebar sidebar-fixed" id="sidebar">

					<ul class="nav nav-list">
						<li class="<?php echo $act_dashboard; ?>">
							<a href="<?php echo $sub_folder; ?>">
								<span style="display:inline-block; vertical-align:middle; min-width:25px; margin-right:2px; text-align:center; padding-bottom:5px;"><img src="<?php echo $sub_folder; ?>assets/icons/Icons-Home.png"></span>

								<span class="menu-text"> Home </span>
							</a>
						</li>

						<li class="<?php echo $act_curriculum; ?>">
							<a href="<?php echo $sub_folder; ?>Curriculum/">
								<span style="display:inline-block; vertical-align:middle; min-width:25px; margin-right:2px; text-align:center; padding-bottom:5px;"><img src="<?php echo $sub_folder; ?>assets/icons/Icons-Curriculum.png"></span>
                <span class="menu-text"> Curriculum </span>
							</a>
						</li>

						<li class="<?php echo $act_planners; ?>">
							<a href="<?php echo $sub_folder; ?>Planners/">
								<span style="display:inline-block; vertical-align:middle; min-width:25px; margin-right:2px; text-align:center; padding-bottom:5px;"><img src="<?php echo $sub_folder; ?>assets/icons/Icons-Planners.png"></span>
								<span class="menu-text"> Planners </span>
							</a>
						</li>

						<li class="<?php echo $act_resources; ?>">
							<a href="<?php echo $sub_folder; ?>Resources/">
								<span style="display:inline-block; vertical-align:middle; min-width:25px; margin-right:2px; text-align:center; padding-bottom:5px;"><img src="<?php echo $sub_folder; ?>assets/icons/Icons-Resources.png"></span>
								<span class="menu-text" > Resources </span>
							</a>
						</li> 

						<li class="<?php echo $act_products_and_services; ?>">
							<a href="<?php echo $sub_folder; ?>Products-and-Services/">
								<span style="display:inline-block; vertical-align:middle; min-width:25px; margin-right:2px; text-align:center; padding-bottom:5px;"><img src="<?php echo $sub_folder; ?>assets/icons/Icons-Products.png"></span>
								<span class="menu-text"> Products & Services </span>
							</a>
						</li>    
            
						<li class="<?php echo $act_events; ?>">
							<a href="<?php echo $sub_folder; ?>Events/">
								<span style="display:inline-block; vertical-align:middle; min-width:25px; margin-right:2px; text-align:center; padding-bottom:5px;"><img src="<?php echo $sub_folder; ?>assets/icons/Icons-Events.png"></span>
								<span class="menu-text"> Events </span>
							</a>
						</li>               
            
						<li class="<?php echo $act_school_network; ?>">
							<a href="<?php echo $sub_folder; ?>School-Network/">
								<span style="display:inline-block; vertical-align:middle; min-width:25px; margin-right:2px; text-align:center; padding-bottom:5px;"><img src="<?php echo $sub_folder; ?>assets/icons/Icons-SchoolNetwork.png"></span>
								<span class="menu-text"> School Network </span>
							</a>
						</li>                        

						<li class="<?php echo $act_school_support; ?>">
							<a href="<?php echo $sub_folder; ?>Support-and-Coaching/">
								<span style="display:inline-block; vertical-align:middle; min-width:25px; margin-right:2px; text-align:center; padding-bottom:5px;"><img src="<?php echo $sub_folder; ?>assets/icons/Icons-Support.png"></span>
								<span class="menu-text"> Support & Coaching </span>
							</a>
						</li>                        

	
<!--						<li class="<?php echo $act_school_support; ?>">
							<a href="#" class="dropdown-toggle">
								<i class="icon-exclamation-sign"></i>
								<span class="menu-text"> Support & Coaching </span>

								<b class="arrow icon-angle-down"></b>
							</a>

              <ul class="submenu">
                <li class="<?php echo $act_support_and_coaching_faq; ?>">
                  <a href="<?php echo $sub_folder; ?>Support-and-Coaching/FAQ/">
                    <i class="icon-double-angle-right"></i>
                    FAQ
                  </a>
                </li>
                
                <li class="<?php echo $act_support_and_coaching_tutorials; ?>">
                  <a href="<?php echo $sub_folder; ?>Support-and-Coaching/Tutorials/">
                    <i class="icon-double-angle-right"></i>
                    Tutorials
                  </a>
                </li>   
                
                <li class="<?php echo $act_support_and_coaching_contact_support; ?>">
                  <a href="<?php echo $sub_folder; ?>Support-and-Coaching/Contact-Support/">
                    <i class="icon-double-angle-right"></i>
                    Contact Support
                  </a>
                </li>                                
                
              </ul>

						</li>
-->
					</ul><!-- /.nav-list -->

					<div class="sidebar-collapse" id="sidebar-collapse">
						<i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
					</div>

				</div>

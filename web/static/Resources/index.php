<?php
  
  include($_SERVER['DOCUMENT_ROOT'].'/assets/inc/global_variables.php');
  
  $this_page = "Resources";
  $page_title = $this_page." | ".$_9rlabs_company_name;
  
  $breadcrumbs = "<li class=\"active\">".$this_page."</li>";

  $page_specific_css = "
    <link rel=\"stylesheet\" href=\"/assets/css/jquery-ui-1.10.3.custom.min.css\" />
		<link rel=\"stylesheet\" href=\"/assets/css/chosen.css\" />
		<link rel=\"stylesheet\" href=\"/assets/css/datepicker.css\" />
		<link rel=\"stylesheet\" href=\"/assets/css/bootstrap-timepicker.css\" />
		<link rel=\"stylesheet\" href=\"/assets/css/daterangepicker.css\" />
		<link rel=\"stylesheet\" href=\"/assets/css/colorpicker.css\" />";

  $page_specific_js = "
		<script src=\"/assets/js/chosen.jquery.min.js\"></script>  
		<script src=\"/assets/js/bootstrap-tag.min.js\"></script>    
  ";

  include($_SERVER['DOCUMENT_ROOT'].'/assets/view/sections/header.php');

  include($_SERVER['DOCUMENT_ROOT'].'/assets/view/sections/navbar.php');
  

?>

    <div class="main-container" id="main-container">

      <div class="main-container-inner">
        <a class="menu-toggler" id="menu-toggler" href="#">
          <span class="menu-text"></span>
        </a>

        <?php include($_SERVER['DOCUMENT_ROOT'].'/assets/view/sections/sidebar.php'); ?>  

        <div class="main-content">

          <?php include($_SERVER['DOCUMENT_ROOT'].'/assets/view/sections/breadcrumbs.php'); ?> 

          <div class="page-content">

            <div class="space-12"></div>
            <div class="space-12"></div>
            <div class="space-12"></div>
            <div class="space-12"></div>
            <div class="space-12"></div>
            <div class="space-12"></div>

            <div class="page-header">
              <h1 style="color:#1c69a5;"><?php echo $this_page; ?></h1>
            </div><!-- /.page-header -->

            <div class="row">
              <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
                
                <?php include($_SERVER['DOCUMENT_ROOT'].'/assets/view/content/resources.php'); ?> 
                

<?php
  
  include($_SERVER['DOCUMENT_ROOT'].'/assets/inc/global_variables.php');
  
  $this_page = "Events";
  $page_title = $this_page." | ".$_9rlabs_company_name;
  
  $breadcrumbs = "<li class=\"active\">".$this_page."</li>";
  
  $page_specific_css = "<link rel=\"stylesheet\" href=\"/assets/css/fullcalendar.css\" />";

  include($_SERVER['DOCUMENT_ROOT'].'/assets/view/sections/header.php');

  include($_SERVER['DOCUMENT_ROOT'].'/assets/view/sections/navbar.php');

?>

    <div class="main-container" id="main-container">

      <div class="main-container-inner">
        <a class="menu-toggler" id="menu-toggler" href="#">
          <span class="menu-text"></span>
        </a>

        <?php include($_SERVER['DOCUMENT_ROOT'].'/assets/view/sections/sidebar.php'); ?>  

        <div class="main-content">

          <?php include($_SERVER['DOCUMENT_ROOT'].'/assets/view/sections/breadcrumbs.php'); ?> 

          <div class="page-content">

            <div class="space-12"></div>
            <div class="space-12"></div>
            <div class="space-12"></div>
            <div class="space-12"></div>
            <div class="space-12"></div>
            <div class="space-12"></div>

            <div class="page-header">
              <h1 style="color:#1c69a5;">Events</h1>
            </div><!-- /.page-header -->

            <div class="row">
              <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
                
                <?php include($_SERVER['DOCUMENT_ROOT'].'/assets/view/content/events.php'); ?> 
                
                <!-- PAGE CONTENT ENDS -->
              </div><!-- /.col -->
            </div><!-- /.row -->
          </div><!-- /.page-content -->
        </div><!-- /.main-content -->

      </div><!-- /.main-container-inner -->

      <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="icon-double-angle-up icon-only bigger-110"></i>
      </a>
    </div><!-- /.main-container -->

    <!-- basic scripts -->

    <!--[if !IE]> -->

    <script type="text/javascript">
      window.jQuery || document.write("<script src='/assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
    </script>

    <!-- <![endif]-->

    <!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='/assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->

    <script type="text/javascript">
      if("ontouchend" in document) document.write("<script src='/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
    </script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/typeahead-bs2.min.js"></script>

    <!-- page specific plugin scripts -->

    <script src="/assets/js/jquery-ui-1.10.3.custom.min.js"></script>
    <script src="/assets/js/jquery.ui.touch-punch.min.js"></script>
    <script src="/assets/js/fullcalendar.min.js"></script>
    <script src="/assets/js/bootbox.min.js"></script>

    <!-- ace scripts -->

    <script src="/assets/js/ace-elements.min.js"></script>
    <script src="/assets/js/ace.min.js"></script>

    <!-- inline scripts related to this page -->

    <script type="text/javascript">
      jQuery(function($) {

/* initialize the external events
  -----------------------------------------------------------------*/

  $('#external-events div.external-event').each(function() {

    // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
    // it doesn't need to have a start or end
    var eventObject = {
      title: $.trim($(this).text()) // use the element's text as the event title
    };

    // store the Event Object in the DOM element so we can get to it later
    $(this).data('eventObject', eventObject);

    // make the event draggable using jQuery UI
    $(this).draggable({
      zIndex: 999,
      revert: true,      // will cause the event to go back to its
      revertDuration: 0  //  original position after the drag
    });
    
  });




  /* initialize the calendar
  -----------------------------------------------------------------*/

  var date = new Date();
  var d = date.getDate();
  var m = date.getMonth();
  var y = date.getFullYear();

  
  var calendar = $('#calendar').fullCalendar({
     buttonText: {
      prev: '<i class="icon-chevron-left"></i>',
      next: '<i class="icon-chevron-right"></i>'
    },
  
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay'
    },
    events: [
    {
      title: 'All Day Event',
      start: new Date(y, m, 1),
      className: 'label-important'
    },
    {
      title: 'Long Event',
      start: new Date(y, m, d-5),
      end: new Date(y, m, d-2),
      className: 'label-success'
    },
    {
      title: 'Some Event',
      start: new Date(y, m, d-3, 16, 0),
      allDay: false
    }]
    ,
    editable: true,
    droppable: true, // this allows things to be dropped onto the calendar !!!
    drop: function(date, allDay) { // this function is called when something is dropped
    
      // retrieve the dropped element's stored Event Object
      var originalEventObject = $(this).data('eventObject');
      var $extraEventClass = $(this).attr('data-class');
      
      
      // we need to copy it, so that multiple events don't have a reference to the same object
      var copiedEventObject = $.extend({}, originalEventObject);
      
      // assign it the date that was reported
      copiedEventObject.start = date;
      copiedEventObject.allDay = allDay;
      if($extraEventClass) copiedEventObject['className'] = [$extraEventClass];
      
      // render the event on the calendar
      // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
      $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
      
      // is the "remove after drop" checkbox checked?
      if ($('#drop-remove').is(':checked')) {
        // if so, remove the element from the "Draggable Events" list
        $(this).remove();
      }
      
    }
    ,
    selectable: true,
    selectHelper: true,
    select: function(start, end, allDay) {
      
      return false;
      
      bootbox.prompt("New Event Title:", function(title) {
        if (title !== null) {
          calendar.fullCalendar('renderEvent',
            {
              title: title,
              start: start,
              end: end,
              allDay: allDay
            },
            true // make the event "stick"
          );
        }
      });
      

      calendar.fullCalendar('unselect');
      
    }
    ,
    eventClick: function(calEvent, jsEvent, view) {
      
      return fale;

      var form = $("<form class='form-inline'><label>Change event name &nbsp;</label></form>");
      form.append("<input class='middle' autocomplete=off type=text value='" + calEvent.title + "' /> ");
      form.append("<button type='submit' class='btn btn-sm btn-success'><i class='icon-ok'></i> Save</button>");
      
      var div = bootbox.dialog({
        message: form,
      
        buttons: {
          "delete" : {
            "label" : "<i class='icon-trash'></i> Delete Event",
            "className" : "btn-sm btn-danger",
            "callback": function() {
              calendar.fullCalendar('removeEvents' , function(ev){
                return (ev._id == calEvent._id);
              })
            }
          } ,
          "close" : {
            "label" : "<i class='icon-remove'></i> Close",
            "className" : "btn-sm"
          } 
        }

      });
      
      form.on('submit', function(){
        calEvent.title = form.find("input[type=text]").val();
        calendar.fullCalendar('updateEvent', calEvent);
        div.modal("hide");
        return false;
      });
      
    
      //console.log(calEvent.id);
      //console.log(jsEvent);
      //console.log(view);

      // change the border color just for fun
      //$(this).css('border-color', 'red');

    }
    
  });


})
    </script>
  </body>
</html>
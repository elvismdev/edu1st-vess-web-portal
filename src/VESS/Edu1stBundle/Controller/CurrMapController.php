<?php

namespace VESS\Edu1stBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class CurrMapController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $domains = $em->getRepository('VESSEdu1stBundle:CurrDomain')->findBy(array(), array('name' => 'asc'));

        foreach ($domains as $domain) {
            $query = $em->createQuery('SELECT ag FROM VESSEdu1stBundle:CurrAgeGroup ag
                WHERE ag.domains = :did1 OR ag.domains LIKE :did2 OR ag.domains LIKE :did3 OR ag.domains LIKE :did4')
<<<<<<< HEAD
                ->setParameter('did1', '[' . $domain->getId() . ']')
                ->setParameter('did2', '[' . $domain->getId() . ',%')
                ->setParameter('did3', '%,' . $domain->getId() . ',%')
                ->setParameter('did4', '%,' . $domain->getId() . ']');
=======
            ->setParameter('did1', '['.$domain->getId().']')
            ->setParameter('did2', '['.$domain->getId().',%')
            ->setParameter('did3', '%,'.$domain->getId().',%')
            ->setParameter('did4', '%,'.$domain->getId().']');
>>>>>>> knp-snappy
            $domain->ages = $query->getResult();
            foreach ($domain->ages as $age) {
                $age->relations = $this->alphabeticalOrder($em->getRepository('VESSEdu1stBundle:CurrDisciplineAreaAreaGroup')->findByCurrAgeGroup($age->getId()));
                foreach ($age->relations as $r) {
                    $discipline = $r->getCurrDisciplineArea();
                    $discipline->relations = $this->alphabeticalOrder($em->getRepository('VESSEdu1stBundle:CurrAreaItemAreaGroupDomain')->findByCurrDisciplineArea($discipline->getId()));
                    foreach ($discipline->relations as $re) {
                        $item = $re->getCurrAreaItem();

                        $query = $em->createQuery('SELECT t FROM VESSEdu1stBundle:CurrTheme t WHERE t.id IN (:ids)')
                        ->setParameter('ids', json_decode($re->getThemes()));
                        $item->currthemes[$re->getCurrDomain()->getId() . $re->getCurrAgeGroup()->getId() . $re->getCurrDisciplineArea()->getId()] = $query->getResult();

                        $query = $em->createQuery('SELECT c FROM VESSEdu1stBundle:CurrCountry c WHERE c.id IN (:ids)')
                        ->setParameter('ids', json_decode($re->getCountries()));
                        $item->currcountries[$re->getCurrDomain()->getId() . $re->getCurrAgeGroup()->getId() . $re->getCurrDisciplineArea()->getId()] = $query->getResult();

                        $query = $em->createQuery('SELECT a FROM VESSEdu1stBundle:CurrArtisticFocus a WHERE a.id IN (:ids)')
                        ->setParameter('ids', json_decode($re->getArtisticFocus()));
                        $item->currartisticfocus[$re->getCurrDomain()->getId() . $re->getCurrAgeGroup()->getId() . $re->getCurrDisciplineArea()->getId()] = $query->getResult();
                    }
                }
            }
        }

        $multilenguage = $em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue();

        return $this->render('VESSEdu1stBundle:CurrMap:index.html.twig', array(
            'domains' => $domains,
            'multilenguage' => $multilenguage
            ));
    }

    private function alphabeticalOrder($list)
    {
        usort($list, array($this, 'order'));

        return $list;
    }

    private function order($node1, $node2)
    {
        switch (get_class($node1)) {
            case 'VESS\Edu1stBundle\Entity\CurrDisciplineAreaAreaGroup':
            $node1 = $node1->getCurrDisciplineArea();
            $node2 = $node2->getCurrDisciplineArea();
            break;
            case 'VESS\Edu1stBundle\Entity\CurrAreaItemAreaGroupDomain':
            $node1 = $node1->getCurrAreaItem();
            $node2 = $node2->getCurrAreaItem();
            break;
        }

        if ($node1->getName() == $node2->getName()) {
            return 0;
        }

        return (strcasecmp($node1->getName(), $node2->getName()) > 0) ? 1 : -1;
    }

    public function pdfAction()
    {
        $em = $this->getDoctrine()->getManager();

        $themes = $em->getRepository('VESSEdu1stBundle:CurrTheme')->findAll();
        $countries = $em->getRepository('VESSEdu1stBundle:CurrCountry')->findAll();
        $afs = $em->getRepository('VESSEdu1stBundle:CurrArtisticFocus')->findAll();
        $domains = $em->getRepository('VESSEdu1stBundle:CurrDomain')->findBy(array(), array('name' => 'asc'));

        foreach ($domains as $domain) {
            $query = $em->createQuery('SELECT ag FROM VESSEdu1stBundle:CurrAgeGroup ag
                WHERE ag.domains = :did1 OR ag.domains LIKE :did2 OR ag.domains LIKE :did3 OR ag.domains LIKE :did4')
<<<<<<< HEAD
                ->setParameter('did1', '[' . $domain->getId() . ']')
                ->setParameter('did2', '[' . $domain->getId() . ',%')
                ->setParameter('did3', '%,' . $domain->getId() . ',%')
                ->setParameter('did4', '%,' . $domain->getId() . ']');
=======
            ->setParameter('did1', '['.$domain->getId().']')
            ->setParameter('did2', '['.$domain->getId().',%')
            ->setParameter('did3', '%,'.$domain->getId().',%')
            ->setParameter('did4', '%,'.$domain->getId().']');
>>>>>>> knp-snappy
            $domain->ages = $query->getResult();
            foreach ($domain->ages as $age) {
                $age->relations = $this->alphabeticalOrder($em->getRepository('VESSEdu1stBundle:CurrDisciplineAreaAreaGroup')->findByCurrAgeGroup($age->getId()));
                foreach ($age->relations as $r) {
                    $discipline = $r->getCurrDisciplineArea();
                    $discipline->relations = $this->alphabeticalOrder($em->getRepository('VESSEdu1stBundle:CurrAreaItemAreaGroupDomain')->findByCurrDisciplineArea($discipline->getId()));
                    foreach ($discipline->relations as $re) {
                        $item = $re->getCurrAreaItem();

                        $query = $em->createQuery('SELECT t FROM VESSEdu1stBundle:CurrTheme t WHERE t.id IN (:ids)')
                            ->setParameter('ids', json_decode($re->getThemes()));
                        $item->currthemes[$re->getCurrDomain()->getId() . $re->getCurrAgeGroup()->getId() . $re->getCurrDisciplineArea()->getId()] = $query->getResult();

                        $query = $em->createQuery('SELECT c FROM VESSEdu1stBundle:CurrCountry c WHERE c.id IN (:ids)')
                            ->setParameter('ids', json_decode($re->getCountries()));
                        $item->currcountries[$re->getCurrDomain()->getId() . $re->getCurrAgeGroup()->getId() . $re->getCurrDisciplineArea()->getId()] = $query->getResult();

                        $query = $em->createQuery('SELECT a FROM VESSEdu1stBundle:CurrArtisticFocus a WHERE a.id IN (:ids)')
                            ->setParameter('ids', json_decode($re->getArtisticFocus()));
                        $item->currartisticfocus[$re->getCurrDomain()->getId() . $re->getCurrAgeGroup()->getId() . $re->getCurrDisciplineArea()->getId()] = $query->getResult();
                    }
                }
            }
        }

        $html = $this->renderView('VESSEdu1stBundle:CurrMap:pdf.html.twig', array(
            'domains' => $domains,
            'themes' => $themes,
            'countries' => $countries,
            'afs' => $afs
            ));

<<<<<<< HEAD
        $mpdfService = $this->get('tfox.mpdfport');
        $mpdfService->generatePdf($html);
        $response = $mpdfService->generatePdfResponse($html);

=======
        // return new Response(
        //     $this->get('knp_snappy.image')->getOutputFromHtml($html),
        //     200,
        //     array(
        //         'Content-Type'          => 'image/jpg',
        //         'Content-Disposition'   => 'filename="image.jpg"'
        //     )
        // );

        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html,
                array('orientation' => 'Landscape',
                        'default-header' => false,
                        'page-size' => 'A4')),
            200,
            array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'attachment; filename="out.pdf"',
                )
            );

        // return new Response($html);
>>>>>>> knp-snappy
    }
}

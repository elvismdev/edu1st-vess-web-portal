<?php

namespace VESS\Edu1stBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\Common\Collections\ArrayCollection;

use VESS\Edu1stBundle\Entity\CurrAreaItem;
use VESS\Edu1stBundle\Entity\CurrAreaItemAreaGroupDomain;
use VESS\Edu1stBundle\Form\CurrAreaItemType;

/**
 * CurrAreaItem controller.
 *
 */
class CurrAreaItemController extends Controller
{

    /**
     * Lists all CurrAreaItem entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('VESSEdu1stBundle:CurrAreaItem')->findBy(array(), array('name' => 'asc'));

        $multilenguage = $em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue();

        return $this->render('VESSEdu1stBundle:CurrAreaItem:index.html.twig', array(
            'entities' => $entities,
            'multilenguage' => $multilenguage
        ));
    }

    /**
     * Creates a new CurrAreaItem entity.
     *
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = new CurrAreaItem();

        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($entity);

            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'notice',
                'Standard were created succefully'
            );

            return $this->redirect($this->generateUrl('currmap'));
        }

        $multilenguage = $em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue();

        return $this->render('VESSEdu1stBundle:CurrAreaItem:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
            'multilenguage' => $multilenguage
        ));
    }

    /**
     * Creates a form to create a CurrAreaItem entity.
     *
     * @param CurrAreaItem $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(CurrAreaItem $entity)
    {
        $form = $this->createForm(new CurrAreaItemType($this->getDoctrine()->getManager()), $entity, array(
            'action' => $this->generateUrl('currareaitem_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new CurrAreaItem entity.
     *
     */
    public function newAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entity = new CurrAreaItem();
        $form = $this->createCreateForm($entity);

        $multilenguage = $em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue();

        return $this->render('VESSEdu1stBundle:CurrAreaItem:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
            'multilenguage' => $multilenguage
        ));
    }

    /**
     * Finds and displays a CurrAreaItem entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('VESSEdu1stBundle:CurrAreaItem')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CurrAreaItem entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        $multilenguage = $em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue();

        return $this->render('VESSEdu1stBundle:CurrAreaItem:show.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
            'multilenguage' => $multilenguage
        ));
    }

    /**
     * Displays a form to edit an existing CurrAreaItem entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $relation = $em->getRepository('VESSEdu1stBundle:CurrAreaItemAreaGroupDomain')->find($id);

        if (!$relation) {
            throw $this->createNotFoundException('Unable to find CurrAreaItemAreaGroupDomain entity.');
        }

        $entity = $relation->getCurrAreaItem();

        $themes = $countries = $afs = array();

        $array = json_decode($relation->getThemes());
        foreach ($array as $v) {
            $theme = $em->getRepository('VESSEdu1stBundle:CurrTheme')->find($v);

            if ($theme) {
                $themes[] = $theme;
            }
        }
        $entity->setThemes($themes);

        $array = json_decode($relation->getCountries());
        foreach ($array as $v) {
            $country = $em->getRepository('VESSEdu1stBundle:CurrCountry')->find($v);

            if ($country) {
                $countries[] = $country;
            }
        }
        $entity->setCountries($countries);

        $array = json_decode($relation->getArtisticFocus());
        foreach ($array as $v) {
            $af = $em->getRepository('VESSEdu1stBundle:CurrArtisticFocus')->find($v);

            if ($af) {
                $afs[] = $af;
            }
        }
        $entity->setArtisticFocus($afs);

        $entity->setCurrDisciplineAreaCascade($relation->getCurrDomain()->getId() . '-' . $relation->getCurrAgeGroup()->getId() . '-' . $relation->getCurrDisciplineArea()->getId());

        $editForm = $this->createEditForm($entity, $id);
        $deleteForm = $this->createDeleteForm($entity->getId());

        $multilenguage = $em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue();

        return $this->render('VESSEdu1stBundle:CurrAreaItem:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'multilenguage' => $multilenguage
        ));
    }

    /**
     * Displays a form to edit an existing CurrAreaItem entity.
     *
     */
    public function cleaneditAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('VESSEdu1stBundle:CurrAreaItem')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CurrAreaItem entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($entity->getId());

        $multilenguage = $em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue();

        return $this->render('VESSEdu1stBundle:CurrAreaItem:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'multilenguage' => $multilenguage
        ));
    }

    /**
     * Creates a form to edit a CurrAreaItem entity.
     *
     * @param CurrAreaItem $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(CurrAreaItem $entity, $id=null)
    {
        $mode = 'edit';
        $url = 'currareaitem_update';
        if (!$id) {
            $mode = 'new';
            $url = 'currareaitem_cleanupdate';
            $id = $entity->getId();
        }

        $form = $this->createForm(new CurrAreaItemType($this->getDoctrine()->getManager(), $mode), $entity, array(
            'action' => $this->generateUrl($url, array('id' => $id)),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing CurrAreaItem entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $relation = $em->getRepository('VESSEdu1stBundle:CurrAreaItemAreaGroupDomain')->find($id);

        if (!$relation) {
            throw $this->createNotFoundException('Unable to find CurrAreaItemAreaGroupDomain entity.');
        }

        $entity = $relation->getCurrAreaItem();

        $themes = $countries = $afs = array();

        $array = json_decode($relation->getThemes());
        foreach ($array as $v) {
            $theme = $em->getRepository('VESSEdu1stBundle:CurrTheme')->find($v);

            if ($theme) {
                $themes[] = $theme;
            }
        }
        $entity->setThemes(new ArrayCollection($themes));

        $array = json_decode($relation->getCountries());
        foreach ($array as $v) {
            $country = $em->getRepository('VESSEdu1stBundle:CurrCountry')->find($v);

            if ($country) {
                $countries[] = $country;
            }
        }
        $entity->setCountries(new ArrayCollection($countries));

        $array = json_decode($relation->getArtisticFocus());
        foreach ($array as $v) {
            $af = $em->getRepository('VESSEdu1stBundle:CurrArtisticFocus')->find($v);

            if ($af) {
                $afs[] = $af;
            }
        }
        $entity->setArtisticFocus(new ArrayCollection($afs));

        $fr = $request->get('vess_edu1stbundle_currareaitem');

        $deleteForm = $this->createDeleteForm($entity->getId());
        $editForm = $this->createEditForm($entity, $id);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            $array = array();
            foreach ($entity->getThemes()->getValues() as $v) {
                $array[] = $v->getId();
            }
            $relation->setThemes(json_encode($array));

            $array = array();
            foreach ($entity->getCountries()->getValues() as $v) {
                $array[] = $v->getId();
            }
            $relation->setCountries(json_encode($array));

            $array = array();
            foreach ($entity->getArtisticFocus()->getValues() as $v) {
                $array[] = $v->getId();
            }
            $relation->setArtisticFocus(json_encode($array));

            $fr = explode('-', $fr['currDisciplineAreaCascade']);

            $relation->setCurrAreaItem($entity);
            $relation->setCurrDisciplineArea($em->getRepository('VESSEdu1stBundle:CurrDisciplineArea')->find($fr[2]));
            $relation->setCurrAgeGroup($em->getRepository('VESSEdu1stBundle:CurrAgeGroup')->find($fr[1]));
            $relation->setCurrDomain($em->getRepository('VESSEdu1stBundle:CurrDomain')->find($fr[0]));

            $em->persist($relation);
            $em->persist($entity);

            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'notice',
                'Standard were updated succefully'
            );

            return $this->redirect($this->generateUrl('currmap'));
        }

        $multilenguage = $em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue();

        return $this->render('VESSEdu1stBundle:CurrAreaItem:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'multilenguage' => $multilenguage
        ));
    }

    /**
     * Edits an existing CurrAreaItem entity.
     *
     */
    public function cleanupdateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('VESSEdu1stBundle:CurrAreaItem')->find($id);

        $deleteForm = $this->createDeleteForm($entity->getId());
        $editForm = $this->createEditForm($entity, $id);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            $em->persist($entity);

            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'notice',
                'Standard were updated succefully'
            );

            return $this->redirect($this->generateUrl('currmap'));
        }

        $multilenguage = $em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue();

        return $this->render('VESSEdu1stBundle:CurrAreaItem:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'multilenguage' => $multilenguage
        ));
    }

    /**
     * Deletes a CurrAreaItem entity.
     *
     */
    /*public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('VESSEdu1stBundle:CurrAreaItem')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find CurrAreaItem entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('currareaitem'));
    }*/

    /**
     * Deletes a CurrAreaItem entity.
     *
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('VESSEdu1stBundle:CurrAreaItem')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CurrAreaItem entity.');
        }

        $em->remove($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add(
            'notice',
            'Standard were deleted succefully'
        );

        return $this->redirect($this->generateUrl('currmap'));
    }

    /**
     * Creates a form to delete a CurrAreaItem entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('currareaitem_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm();
    }
}

<?php

namespace VESS\Edu1stBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\Common\Collections\ArrayCollection;

use VESS\Edu1stBundle\Entity\CurrAgeGroup;
use VESS\Edu1stBundle\Form\CurrAgeGroupType;

/**
 * CurrAgeGroup controller.
 *
 */
class CurrAgeGroupController extends Controller
{

    /**
     * Lists all CurrAgeGroup entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('VESSEdu1stBundle:CurrAgeGroup')->findAll();

        return $this->render('VESSEdu1stBundle:CurrAgeGroup:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new CurrAgeGroup entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new CurrAgeGroup();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $array = array();
            foreach ($entity->getDomains()->getValues() as $v) {
                $array[] = $v->getId();
            }
            $entity->setDomains(json_encode($array));

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('curragegroup_show', array('id' => $entity->getId())));
        }

        return $this->render('VESSEdu1stBundle:CurrAgeGroup:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a CurrAgeGroup entity.
     *
     * @param CurrAgeGroup $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(CurrAgeGroup $entity)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(new CurrAgeGroupType($em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue()), $entity, array(
            'action' => $this->generateUrl('curragegroup_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new CurrAgeGroup entity.
     *
     */
    public function newAction()
    {
        $entity = new CurrAgeGroup();
        $form   = $this->createCreateForm($entity);

        return $this->render('VESSEdu1stBundle:CurrAgeGroup:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a CurrAgeGroup entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('VESSEdu1stBundle:CurrAgeGroup')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CurrAgeGroup entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('VESSEdu1stBundle:CurrAgeGroup:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing CurrAgeGroup entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('VESSEdu1stBundle:CurrAgeGroup')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CurrAgeGroup entity.');
        }

        $domains = array();

        $array = json_decode($entity->getDomains());
        foreach ($array as $v) {
            $domain = $em->getRepository('VESSEdu1stBundle:CurrDomain')->find($v);

            if ($domain) {
                $domains[] = $domain;
            }
        }
        $entity->setDomains($domains);

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('VESSEdu1stBundle:CurrAgeGroup:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a CurrAgeGroup entity.
    *
    * @param CurrAgeGroup $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(CurrAgeGroup $entity)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(new CurrAgeGroupType($em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue()), $entity, array(
            'action' => $this->generateUrl('curragegroup_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing CurrAgeGroup entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('VESSEdu1stBundle:CurrAgeGroup')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CurrAgeGroup entity.');
        }

        $domains = array();

        $array = json_decode($entity->getDomains());
        foreach ($array as $v) {
            $domain = $em->getRepository('VESSEdu1stBundle:CurrDomain')->find($v);

            if ($domain) {
                $domains[] = $domain;
            }
        }
        $entity->setDomains(new ArrayCollection($domains));

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            $array = array();
            foreach ($entity->getDomains()->getValues() as $v) {
                $array[] = $v->getId();
            }
            $entity->setDomains(json_encode($array));

            $em->flush();

            return $this->redirect($this->generateUrl('curragegroup_edit', array('id' => $id)));
        }

        return $this->render('VESSEdu1stBundle:CurrAgeGroup:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a CurrAgeGroup entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('VESSEdu1stBundle:CurrAgeGroup')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find CurrAgeGroup entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('curragegroup'));
    }

    /**
     * Creates a form to delete a CurrAgeGroup entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('curragegroup_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}

<?php

namespace VESS\Edu1stBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use VESS\Edu1stBundle\Entity\User;
use VESS\Edu1stBundle\Form\UserType;

/**
 * User controller.
 *
 */
class UserController extends Controller
{

    /**
     * Lists all User entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('VESSEdu1stBundle:User')->findAll();

        return $this->render('VESSEdu1stBundle:User:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Creates a new User entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new User();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $entity->setCode($entity->getPassword());
            $this->setSecurePassword($entity);

            // Timestamp
            $entity->setLastupdate(date('Y-m-d H:i:s'));

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('user_show', array('id' => $entity->getId())));
        }

        return $this->render('VESSEdu1stBundle:User:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a User entity.
     *
     * @param User $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(User $entity)
    {
        $form = $this->createForm(new UserType($user = $this->get('security.context')->getToken()->getUser()), $entity, array(
            'action' => $this->generateUrl('user_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new User entity.
     *
     */
    public function newAction()
    {
        $entity = new User();
        $form = $this->createCreateForm($entity);

        return $this->render('VESSEdu1stBundle:User:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a User entity.
     *
     */
    public function showAction($id)
    {
        $user = $this->get('security.context')->getToken()->getUser();

        if ($user->getId() != $id && $user->getRol() != 'ROLE_ADMIN')
            return $this->redirect($this->generateUrl('user_profile', array('id' => $user->getId())));

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('VESSEdu1stBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('VESSEdu1stBundle:User:show.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     */
    public function editAction($id)
    {
        $user = $this->get('security.context')->getToken()->getUser();

        if ($user->getId() != $id && $user->getRol() != 'ROLE_ADMIN')
            return $this->redirect($this->generateUrl('user_edit', array('id' => $user->getId())));

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('VESSEdu1stBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('VESSEdu1stBundle:User:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a User entity.
     *
     * @param User $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(User $entity)
    {
        $form = $this->createForm(new UserType($user = $this->get('security.context')->getToken()->getUser()), $entity, array(
            'action' => $this->generateUrl('user_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing User entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $user = $this->get('security.context')->getToken()->getUser();

        if ($user->getId() != $id && $user->getRol() != 'ROLE_ADMIN')
            return $this->redirect($this->generateUrl('user_profile', array('id' => $user->getId())));

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('VESSEdu1stBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $current_pass = $entity->getPassword();

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            // Changing password if needed
            if ($entity->getPassword() != "") {
                $entity->setCode($entity->getPassword());
                $this->setSecurePassword($entity);
            } else {
                $entity->setPassword($current_pass);
            }

            // Timestamp
            $entity->setLastupdate(date('Y-m-d H:i:s'));

            $em->flush();

            return $this->redirect($this->generateUrl('user_edit', array('id' => $id)));
        }

        return $this->render('VESSEdu1stBundle:User:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a User entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('VESSEdu1stBundle:User')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find User entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('user'));
    }

    /**
     * Creates a form to delete a User entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('user_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm();
    }

    private function setSecurePassword(&$entity)
    {
        $entity->setSalt(md5(time()));
        $encoder = new \Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder('sha512', true, 10);
        $password = $encoder->encodePassword($entity->getPassword(), $entity->getSalt());
        $entity->setPassword($password);
    }

    /*Change password for an user
     *
     *
     */
    public function changepassAction($id)
    {
        $user = $this->get('security.context')->getToken()->getUser();

        if ($user->getId() != $id && $user->getRol() != 'ROLE_ADMIN')
            return $this->redirect($this->generateUrl('user_changepass', array('id' => $user->getId())));

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('VESSEdu1stBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $form = $this->createFormBuilder()
            ->add('current_password', 'password', array('label' => 'current_passwd', 'required' => true))
            ->add('password', 'repeated', array(
                'type' => 'password',
                'invalid_message' => 'The password fields must match.',
                'options' => array('attr' => array('class' => 'password-field')),
                'required' => true,
                'first_options' => array('label' => 'password'),
                'second_options' => array('label' => 'repeat_password')))->getForm();

        return $this->render('VESSEdu1stBundle:User:changePassword.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /*
     * Update pasword for an user
     *
     */
    public function updatepassAction($id, Request $request)
    {
        $user = $this->get('security.context')->getToken()->getUser();

        if ($user->getId() != $id && $user->getRol() != 'ROLE_ADMIN')
            return $this->redirect($this->generateUrl('user_profile', array('id' => $user->getId())));

        $em = $this->getDoctrine()->getManager();

        $form = $request->get('form');

        $current_password = $form['current_password'];
        $new_password = $form['password']['first'];
        $new_password_confirm = $form['password']['second'];

        $entity = $em->getRepository('VESSEdu1stBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Usuario entity.');
        }

        // Check if current password is OK
        $encoder_service = $this->get('security.encoder_factory');
        $encoder = $encoder_service->getEncoder($entity);
        $encoded_pass = $encoder->encodePassword($current_password, $entity->getSalt());

        if ($encoded_pass != $entity->getPassword()) {
            $error = 1;

            $this->get('session')->getFlashBag()->add(
                'notice',
                'current_password_incorrect'
            );
        }

        if ($new_password != $new_password_confirm) {
            $error = 1;

            $this->get('session')->getFlashBag()->add(
                'notice',
                'passwords_are_not_the_same'
            );
        }

        if (!isset($error)) {

            // Changing password
            $entity->setCode($new_password);
            $entity->setPassword($new_password);
            $this->setSecurePassword($entity);

            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'notice',
                'password_change_success'
            );
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->redirect($this->generateUrl('user_profile', array(
            'id' => $entity->getId(),
            'entity' => $entity,
            //'delete_form' => $deleteForm->createView()
        )));
    }
}

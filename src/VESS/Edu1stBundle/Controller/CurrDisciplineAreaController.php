<?php

namespace VESS\Edu1stBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use VESS\Edu1stBundle\Entity\CurrDisciplineArea;
use VESS\Edu1stBundle\Entity\CurrDisciplineAreaAreaGroup;
use VESS\Edu1stBundle\Form\CurrDisciplineAreaType;

/**
 * CurrDisciplineArea controller.
 *
 */
class CurrDisciplineAreaController extends Controller
{

    /**
     * Lists all CurrDisciplineArea entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('VESSEdu1stBundle:CurrDisciplineArea')->findAll();

        $multilenguage = $em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue();

        return $this->render('VESSEdu1stBundle:CurrDisciplineArea:index.html.twig', array(
            'entities' => $entities,
            'multilenguage' => $multilenguage
        ));
    }

    /**
     * Creates a new CurrDisciplineArea entity.
     *
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = new CurrDisciplineArea();

        $fr = $request->get('vess_edu1stbundle_currdisciplinearea');

        $currDiscipplineRelations = array();
        foreach ($fr['currAgeGroupDomain'] as $choice) {
            $f = explode('-', $choice);
            $currDiscipplineRelations[] = new CurrDisciplineAreaAreaGroup($entity, $em->getRepository('VESSEdu1stBundle:CurrAgeGroup')->find($f[0]), $em->getRepository('VESSEdu1stBundle:CurrDomain')->find($f[1]));
        }

        $entity->setName($fr['name']);

        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->persist($entity);

            foreach ($currDiscipplineRelations as $c) {
                $em->persist($c);
            }

            $this->get('session')->getFlashBag()->add(
                'notice',
                'Sub Domain were created succefully'
            );

            $em->flush();

            return $this->redirect($this->generateUrl('currmap'));
        }

        $multilenguage = $em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue();

        return $this->render('VESSEdu1stBundle:CurrDisciplineArea:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
            'multilenguage' => $multilenguage
        ));
    }

    /**
     * Creates a form to create a CurrDisciplineArea entity.
     *
     * @param CurrDisciplineArea $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(CurrDisciplineArea $entity)
    {
        $form = $this->createForm(new CurrDisciplineAreaType($this->getDoctrine()->getManager()), $entity, array(
            'action' => $this->generateUrl('currdisciplinearea_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new CurrDisciplineArea entity.
     *
     */
    public function newAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entity = new CurrDisciplineArea();
        $form = $this->createCreateForm($entity);

        $multilenguage = $em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue();

        return $this->render('VESSEdu1stBundle:CurrDisciplineArea:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
            'multilenguage' => $multilenguage
        ));
    }

    /**
     * Finds and displays a CurrDisciplineArea entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('VESSEdu1stBundle:CurrDisciplineArea')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CurrDisciplineArea entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        $multilenguage = $em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue();

        return $this->render('VESSEdu1stBundle:CurrDisciplineArea:show.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
            'multilenguage' => $multilenguage
        ));
    }

    /**
     * Displays a form to edit an existing CurrDisciplineArea entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('VESSEdu1stBundle:CurrDisciplineArea')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CurrDisciplineArea entity.');
        }

        $relations = $em->getRepository('VESSEdu1stBundle:CurrDisciplineAreaAreaGroup')->findByCurrDisciplineArea($id);

        $list = array();
        foreach ($relations as $r) {
            $list[] = $r->getCurrAgeGroup()->getId() . '-' . $r->getCurrDomain()->getId();
        }
        $entity->setCurrAgeGroupDomain($list);

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        $multilenguage = $em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue();

        return $this->render('VESSEdu1stBundle:CurrDisciplineArea:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'multilenguage' => $multilenguage
        ));
    }

    /**
     * Creates a form to edit a CurrDisciplineArea entity.
     *
     * @param CurrDisciplineArea $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(CurrDisciplineArea $entity)
    {
        $form = $this->createForm(new CurrDisciplineAreaType($this->getDoctrine()->getManager()), $entity, array(
            'action' => $this->generateUrl('currdisciplinearea_update', array('id' => $entity->getId(), '_locale' => $this->getRequest()->getLocale())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing CurrDisciplineArea entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('VESSEdu1stBundle:CurrDisciplineArea')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CurrDisciplineArea entity.');
        }

        $fr = $request->get('vess_edu1stbundle_currdisciplinearea');

        $currDiscipplineRelations = array();
        foreach ($fr['currAgeGroupDomain'] as $choice) {
            $f = explode('-', $choice);
            $currDiscipplineRelations[] = new CurrDisciplineAreaAreaGroup($entity, $em->getRepository('VESSEdu1stBundle:CurrAgeGroup')->find($f[0]), $em->getRepository('VESSEdu1stBundle:CurrDomain')->find($f[1]));
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $relations = $em->getRepository('VESSEdu1stBundle:CurrDisciplineAreaAreaGroup')->findByCurrDisciplineArea($id);

            foreach ($relations as $r) {
                // Drop old relations first
                $em->remove($r);
            }

            foreach ($currDiscipplineRelations as $c) {
                $em->persist($c);
            }

            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'notice',
                'Sub Domain were updated succefully'
            );

            return $this->redirect($this->generateUrl('currmap'));
        }

        $multilenguage = $em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue();

        return $this->render('VESSEdu1stBundle:CurrDisciplineArea:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'multilenguage' => $multilenguage
        ));
    }

    /**
     * Deletes a CurrDisciplineArea entity.
     *
     */
    /*public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('VESSEdu1stBundle:CurrDisciplineArea')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find CurrDisciplineArea entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('currmap'));
    }*/

    /**
     * Deletes a CurrDisciplineArea entity.
     *
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('VESSEdu1stBundle:CurrDisciplineArea')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CurrDisciplineArea entity.');
        }

        $em->remove($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add(
            'notice',
            'Sub Domain were deleted succefully'
        );

        return $this->redirect($this->generateUrl('currmap', array('_locale' => $this->getRequest()->getLocale())));
    }

    /**
     * Creates a form to delete a CurrDisciplineArea entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('currdisciplinearea_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm();
    }
}

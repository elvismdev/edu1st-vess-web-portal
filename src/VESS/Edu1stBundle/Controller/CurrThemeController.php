<?php

namespace VESS\Edu1stBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use VESS\Edu1stBundle\Entity\CurrTheme;
use VESS\Edu1stBundle\Form\CurrThemeType;

/**
 * CurrTheme controller.
 *
 */
class CurrThemeController extends Controller
{

    /**
     * Lists all CurrTheme entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('VESSEdu1stBundle:CurrTheme')->findAll();

        $multilenguage = $em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue();

        return $this->render('VESSEdu1stBundle:CurrTheme:index.html.twig', array(
            'entities' => $entities,
            'multilenguage' => $multilenguage
        ));
    }
    /**
     * Creates a new CurrTheme entity.
     *
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = new CurrTheme();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('currtheme_show', array('id' => $entity->getId())));
        }

        $multilenguage = $em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue();

        return $this->render('VESSEdu1stBundle:CurrTheme:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'multilenguage' => $multilenguage
        ));
    }

    /**
     * Creates a form to create a CurrTheme entity.
     *
     * @param CurrTheme $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(CurrTheme $entity)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(new CurrThemeType($em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue()), $entity, array(
            'action' => $this->generateUrl('currtheme_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new CurrTheme entity.
     *
     */
    public function newAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entity = new CurrTheme();
        $form   = $this->createCreateForm($entity);

        $multilenguage = $em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue();

        return $this->render('VESSEdu1stBundle:CurrTheme:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'multilenguage' => $multilenguage
        ));
    }

    /**
     * Finds and displays a CurrTheme entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('VESSEdu1stBundle:CurrTheme')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CurrTheme entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        $multilenguage = $em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue();

        return $this->render('VESSEdu1stBundle:CurrTheme:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
            'multilenguage' => $multilenguage
        ));
    }

    /**
     * Displays a form to edit an existing CurrTheme entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('VESSEdu1stBundle:CurrTheme')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CurrTheme entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        $multilenguage = $em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue();

        return $this->render('VESSEdu1stBundle:CurrTheme:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'multilenguage' => $multilenguage
        ));
    }

    /**
    * Creates a form to edit a CurrTheme entity.
    *
    * @param CurrTheme $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(CurrTheme $entity)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(new CurrThemeType($em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue()), $entity, array(
            'action' => $this->generateUrl('currtheme_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing CurrTheme entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('VESSEdu1stBundle:CurrTheme')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CurrTheme entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('currtheme_edit', array('id' => $id)));
        }

        $multilenguage = $em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue();

        return $this->render('VESSEdu1stBundle:CurrTheme:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'multilenguage' => $multilenguage
        ));
    }
    /**
     * Deletes a CurrTheme entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('VESSEdu1stBundle:CurrTheme')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find CurrTheme entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('currtheme'));
    }

    /**
     * Creates a form to delete a CurrTheme entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('currtheme_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}

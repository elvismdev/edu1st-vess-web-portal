<?php

namespace VESS\Edu1stBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use VESS\Edu1stBundle\Entity\CurrDomain;
use VESS\Edu1stBundle\Form\CurrDomainType;

/**
 * CurrDomain controller.
 *
 */
class CurrDomainController extends Controller
{

    /**
     * Lists all CurrDomain entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('VESSEdu1stBundle:CurrDomain')->findAll();

        return $this->render('VESSEdu1stBundle:CurrDomain:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new CurrDomain entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new CurrDomain();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('currdomain_show', array('id' => $entity->getId())));
        }

        return $this->render('VESSEdu1stBundle:CurrDomain:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a CurrDomain entity.
     *
     * @param CurrDomain $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(CurrDomain $entity)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(new CurrDomainType($em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue()), $entity, array(
            'action' => $this->generateUrl('currdomain_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new CurrDomain entity.
     *
     */
    public function newAction()
    {
        $entity = new CurrDomain();
        $form   = $this->createCreateForm($entity);

        return $this->render('VESSEdu1stBundle:CurrDomain:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a CurrDomain entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('VESSEdu1stBundle:CurrDomain')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CurrDomain entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('VESSEdu1stBundle:CurrDomain:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing CurrDomain entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('VESSEdu1stBundle:CurrDomain')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CurrDomain entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('VESSEdu1stBundle:CurrDomain:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a CurrDomain entity.
    *
    * @param CurrDomain $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(CurrDomain $entity)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(new CurrDomainType($em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue()), $entity, array(
            'action' => $this->generateUrl('currdomain_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing CurrDomain entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('VESSEdu1stBundle:CurrDomain')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CurrDomain entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('currdomain_edit', array('id' => $id)));
        }

        return $this->render('VESSEdu1stBundle:CurrDomain:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a CurrDomain entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('VESSEdu1stBundle:CurrDomain')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find CurrDomain entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('currdomain'));
    }

    /**
     * Creates a form to delete a CurrDomain entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('currdomain_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}

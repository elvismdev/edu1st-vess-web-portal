<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
namespace VESS\Edu1stBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;

use VESS\Edu1stBundle\Controller\EmailController;

class SecurityController extends Controller
{
    public function loginAction()
    {
        $peticion = $this->getRequest();

        $sesion = $peticion->getSession();

        if ($peticion->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $peticion->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $sesion->get(SecurityContext::AUTHENTICATION_ERROR);
        }
        return $this->render('VESSEdu1stBundle:Security:login.html.twig', array(
            'last_username' => $sesion->get(SecurityContext::LAST_USERNAME),
            'error' => $error,
        ));
    }

    public function nonaccessAction()
    {
        // Log the user out
        $this->get("request")->getSession()->invalidate();
        $this->get("security.context")->setToken(null);

        return $this->redirect($this->generateUrl('Login', array('nonaccess' => true)));
    }

    public function passforgotAction()
    {
        $em = $this->getDoctrine()->getManager();

        $email = $this->get('request')->request->get('email');

        if (!$email)
            return $this->redirect($this->generateUrl('Login'));

        $entity = $em->getRepository('VESSEdu1stBundle:User')->findOneBy(array('email' => $email));

        if (!$entity) {
            return $this->redirect($this->generateUrl('Login'));
        }

        $email = new EmailController($em, $this->get('kernel')->getRootDir());
        $email->EmailRetrievePassword($entity);

        $this->get('twig')->addGlobal('emailsent', $email);

        return $this->render('VESSEdu1stBundle:Security:login.html.twig', array(
            'last_username' => '',
            'error' => '',
            'emailsent' => $email
        ));
    }
}
?>
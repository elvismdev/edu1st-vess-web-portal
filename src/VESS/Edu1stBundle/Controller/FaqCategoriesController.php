<?php

namespace VESS\Edu1stBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use VESS\Edu1stBundle\Entity\FaqCategories;
use VESS\Edu1stBundle\Form\FaqCategoriesType;

/**
 * FaqCategories controller.
 *
 */
class FaqCategoriesController extends Controller
{

    /**
     * Lists all FaqCategories entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('VESSEdu1stBundle:FaqCategories')->findAll();

        foreach ($entities as $category) {
            $category->faqList = $em->getRepository('VESSEdu1stBundle:FaqList')->findByfaqCat($category);
        }

        $multilenguage = $em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue();

        return $this->render('VESSEdu1stBundle:FaqCategories:index.html.twig', array(
            'entities' => $entities,
            'multilenguage' => $multilenguage
        ));
    }
    /**
     * Creates a new FaqCategories entity.
     *
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = new FaqCategories();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('faqcategories'));
        }

        $multilenguage = $em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue();

        return $this->render('VESSEdu1stBundle:FaqCategories:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'multilenguage' => $multilenguage
        ));
    }

    /**
     * Creates a form to create a FaqCategories entity.
     *
     * @param FaqCategories $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(FaqCategories $entity)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(new FaqCategoriesType($em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue()), $entity, array(
            'action' => $this->generateUrl('faqcategories_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new FaqCategories entity.
     *
     */
    public function newAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entity = new FaqCategories();
        $form   = $this->createCreateForm($entity);

        $multilenguage = $em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue();

        return $this->render('VESSEdu1stBundle:FaqCategories:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'multilenguage' => $multilenguage
        ));
    }

    /**
     * Finds and displays a FaqCategories entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('VESSEdu1stBundle:FaqCategories')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find FaqCategories entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        $multilenguage = $em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue();

        return $this->render('VESSEdu1stBundle:FaqCategories:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
            'multilenguage' => $multilenguage
        ));
    }

    /**
     * Displays a form to edit an existing FaqCategories entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('VESSEdu1stBundle:FaqCategories')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find FaqCategories entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        $multilenguage = $em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue();

        return $this->render('VESSEdu1stBundle:FaqCategories:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'multilenguage' => $multilenguage
        ));
    }

    /**
    * Creates a form to edit a FaqCategories entity.
    *
    * @param FaqCategories $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(FaqCategories $entity)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(new FaqCategoriesType($em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue()), $entity, array(
            'action' => $this->generateUrl('faqcategories_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing FaqCategories entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('VESSEdu1stBundle:FaqCategories')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find FaqCategories entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('faqcategories_edit', array('id' => $id)));
        }

        $multilenguage = $em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue();

        return $this->render('VESSEdu1stBundle:FaqCategories:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'multilenguage' => $multilenguage
        ));
    }
    /**
     * Deletes a FaqCategories entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('VESSEdu1stBundle:FaqCategories')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find FaqCategories entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('faqcategories'));
    }

    /**
     * Creates a form to delete a FaqCategories entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('faqcategories_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}

<?php

namespace VESS\Edu1stBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Filesystem\Filesystem;


class EmailController
{

    private $em;
    private $rootDir;

    function __construct($em, $rootDir)
    {
        $this->em = $em;
        $this->rootDir = $rootDir;
    }

    private function getAmazonSES()
    {
        require_once($this->rootDir . '/../vendor/amazonsesmailer/AmazonSESMailer.php');

        $amazonaws = $this->em->getRepository('VESSEdu1stBundle:Amazonaws')->findOneById(1);

        return new \AmazonSESMailer($amazonaws->getAwsId(), $amazonaws->getAwsSecretKey());
    }

    public function EmailRetrievePassword($user)
    {
        $mailer = $this->em->getRepository('VESSEdu1stBundle:Mailer')->findOneBySource('PasswordForgot');

        $amazonaws = $this->em->getRepository('VESSEdu1stBundle:Amazonaws')->findOneById(1);

        $amazonSESMailer = $this->getAmazonSES();
        $amazonSESMailer->ClearReplyTos();
        $amazonSESMailer->AddReplyTo($mailer->getFrom());

        $amazonSESMailer->SetFrom($mailer->getFrom(), $mailer->getFromname());

        // ADD THE TO EMAILS
        $amazonSESMailer->AddAddress($user->getEmail());

        // SUBJECT
        $amazonSESMailer->Subject = $mailer->getSubject();

        // MESSAGE
        $body = $mailer->getBody();

        $body = str_replace('[fullname]', $user->getFullname(), $body);
        $body = str_replace('[code]', $user->getCode(), $body);

        $amazonSESMailer->MsgHtml($body);

        $amazonSESMailer->Send();
    }
}
<?php

namespace VESS\Edu1stBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('VESSEdu1stBundle:Default:index.html.twig');
    }
}

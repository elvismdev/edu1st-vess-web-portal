<?php

namespace VESS\Edu1stBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use VESS\Edu1stBundle\Entity\CurrAreaItemAreaGroupDomain;
use VESS\Edu1stBundle\Form\CurrAreaItemAreaGroupDomainType;

/**
 * CurrAreaItemAreaGroupDomain controller.
 *
 */
class CurrAreaItemAreaGroupDomainController extends Controller
{

    /**
     * Lists all CurrAreaItemAreaGroupDomain entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('VESSEdu1stBundle:CurrAreaItemAreaGroupDomain')->findAll();

        return $this->render('VESSEdu1stBundle:CurrAreaItemAreaGroupDomain:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new CurrAreaItemAreaGroupDomain entity.
     *
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = new CurrAreaItemAreaGroupDomain();

        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $array = array();
            foreach ($entity->getThemes()->getValues() as $v) {
                $array[] = $v->getId();
            }
            $themes = json_encode($array);

            $array = array();
            foreach ($entity->getCountries()->getValues() as $v) {
                $array[] = $v->getId();
            }
            $countries = json_encode($array);

            $array = array();
            foreach ($entity->getArtisticFocus()->getValues() as $v) {
                $array[] = $v->getId();
            }
            $artisticFocus = json_encode($array);

            $fr = $request->get('vess_edu1stbundle_currareaitemareagroupdomain');

            $relations = array();

            foreach ($fr['currDisciplineAreaCascade'] as $relation) {
                $f = explode('-', $relation);
                $relations[] = new CurrAreaItemAreaGroupDomain(
                    $em->getRepository('VESSEdu1stBundle:CurrDomain')->find($f[0]),
                    $em->getRepository('VESSEdu1stBundle:CurrAgeGroup')->find($f[1]),
                    $em->getRepository('VESSEdu1stBundle:CurrDisciplineArea')->find($f[2]),
                    $entity->getCurrAreaItem(),
                    $themes, $countries, $artisticFocus
                );
            }

            foreach ($relations as $relation) {
                $em->persist($relation);
            }

            $em->flush();

            return $this->redirect($this->generateUrl('currmap'));
        }

        $multilenguage = $em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue();

        return $this->render('VESSEdu1stBundle:CurrAreaItemAreaGroupDomain:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'multilenguage' => $multilenguage
        ));
    }

    /**
     * Creates a form to create a CurrAreaItemAreaGroupDomain entity.
     *
     * @param CurrAreaItemAreaGroupDomain $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(CurrAreaItemAreaGroupDomain $entity)
    {
        $form = $this->createForm(new CurrAreaItemAreaGroupDomainType($this->getDoctrine()->getManager()), $entity, array(
            'action' => $this->generateUrl('currareaitemareagroupdomain_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new CurrAreaItemAreaGroupDomain entity.
     *
     */
    public function newAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entity = new CurrAreaItemAreaGroupDomain();
        $form   = $this->createCreateForm($entity);

        $multilenguage = $em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue();

        return $this->render('VESSEdu1stBundle:CurrAreaItemAreaGroupDomain:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'multilenguage' => $multilenguage
        ));
    }

    /**
     * Finds and displays a CurrAreaItemAreaGroupDomain entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('VESSEdu1stBundle:CurrAreaItemAreaGroupDomain')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CurrAreaItemAreaGroupDomain entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        $multilenguage = $em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue();

        return $this->render('VESSEdu1stBundle:CurrAreaItemAreaGroupDomain:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
            'multilenguage' => $multilenguage
        ));
    }

    /**
     * Displays a form to edit an existing CurrAreaItemAreaGroupDomain entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('VESSEdu1stBundle:CurrAreaItemAreaGroupDomain')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CurrAreaItemAreaGroupDomain entity.');
        }

        $themes = $countries = $afs = array();

        $array = json_decode($entity->getThemes());
        foreach ($array as $v) {
            $theme = $em->getRepository('VESSEdu1stBundle:CurrTheme')->find($v);

            if ($theme) {
                $themes[] = $theme;
            }
        }
        $entity->setThemes($themes);

        $array = json_decode($entity->getCountries());
        foreach ($array as $v) {
            $country = $em->getRepository('VESSEdu1stBundle:CurrCountry')->find($v);

            if ($country) {
                $countries[] = $country;
            }
        }
        $entity->setCountries($countries);

        $array = json_decode($entity->getArtisticFocus());
        foreach ($array as $v) {
            $af = $em->getRepository('VESSEdu1stBundle:CurrArtisticFocus')->find($v);

            if ($af) {
                $afs[] = $af;
            }
        }
        $entity->setArtisticFocus($afs);

        $currAreaItemRelations = $em->getRepository('VESSEdu1stBundle:CurrAreaItemAreaGroupDomain')->findByCurrAreaItem($id);

        $relations = array();
        foreach ($currAreaItemRelations as $relation) {
            $relations[] = $relation->getCurrDomain()->getId() . '-' . $relation->getCurrAgeGroup()->getId() . '-' . $relation->getCurrDisciplineArea()->getId();
            $entity->setCurrDisciplineAreaCascade($relations);
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        $multilenguage = $em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue();

        return $this->render('VESSEdu1stBundle:CurrAreaItemAreaGroupDomain:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'multilenguage' => $multilenguage
        ));
    }

    /**
    * Creates a form to edit a CurrAreaItemAreaGroupDomain entity.
    *
    * @param CurrAreaItemAreaGroupDomain $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(CurrAreaItemAreaGroupDomain $entity)
    {
        $form = $this->createForm(new CurrAreaItemAreaGroupDomainType($this->getDoctrine()->getManager()), $entity, array(
            'action' => $this->generateUrl('currareaitemareagroupdomain_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing CurrAreaItemAreaGroupDomain entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('VESSEdu1stBundle:CurrAreaItemAreaGroupDomain')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CurrAreaItemAreaGroupDomain entity.');
        }

        $themes = $countries = $afs = array();

        $array = json_decode($entity->getThemes());
        foreach ($array as $v) {
            $theme = $em->getRepository('VESSEdu1stBundle:CurrTheme')->find($v);

            if ($theme) {
                $themes[] = $theme;
            }
        }
        $entity->setThemes(new ArrayCollection($themes));

        $array = json_decode($entity->getCountries());
        foreach ($array as $v) {
            $country = $em->getRepository('VESSEdu1stBundle:CurrCountry')->find($v);

            if ($country) {
                $countries[] = $country;
            }
        }
        $entity->setCountries(new ArrayCollection($countries));

        $array = json_decode($entity->getArtisticFocus());
        foreach ($array as $v) {
            $af = $em->getRepository('VESSEdu1stBundle:CurrArtisticFocus')->find($v);

            if ($af) {
                $afs[] = $af;
            }
        }
        $entity->setArtisticFocus(new ArrayCollection($afs));

        $fr = $request->get('vess_edu1stbundle_currareaitemareagroupdomain');

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            $array = array();
            foreach ($entity->getThemes()->getValues() as $v) {
                $array[] = $v->getId();
            }
            $entity->setThemes(json_encode($array));

            $array = array();
            foreach ($entity->getCountries()->getValues() as $v) {
                $array[] = $v->getId();
            }
            $entity->setCountries(json_encode($array));

            $array = array();
            foreach ($entity->getArtisticFocus()->getValues() as $v) {
                $array[] = $v->getId();
            }
            $entity->setArtisticFocus(json_encode($array));

            $relations = $em->getRepository('VESSEdu1stBundle:CurrAreaItemAreaGroupDomain')->findByCurrAreaItem($id);
            foreach ($relations as $r) {
                // Drop old relations first
                $em->remove($r);
            }

            foreach ($fr['currDisciplineAreaCascade'] as $relation) {
                $f = explode('-', $relation);
                $em->persist(new CurrAreaItemAreaGroupDomain(
                    $em->getRepository('VESSEdu1stBundle:CurrDomain')->find($f[0]),
                    $em->getRepository('VESSEdu1stBundle:CurrAgeGroup')->find($f[1]),
                    $em->getRepository('VESSEdu1stBundle:CurrDisciplineArea')->find($f[2]),
                    $entity->getCurrAreaItem(),
                    $entity->getThemes(), $entity->getCountries(), $entity->getArtisticFocus()
                ));
            }

            $em->flush();

            return $this->redirect($this->generateUrl('currmap'));
        }

        $multilenguage = $em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue();

        return $this->render('VESSEdu1stBundle:CurrAreaItemAreaGroupDomain:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'multilenguage' => $multilenguage
        ));
    }
    /**
     * Deletes a CurrAreaItemAreaGroupDomain entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('VESSEdu1stBundle:CurrAreaItemAreaGroupDomain')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find CurrAreaItemAreaGroupDomain entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('currareaitemareagroupdomain'));
    }

    /**
     * Creates a form to delete a CurrAreaItemAreaGroupDomain entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('currareaitemareagroupdomain_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}

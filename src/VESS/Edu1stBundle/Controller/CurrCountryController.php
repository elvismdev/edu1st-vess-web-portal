<?php

namespace VESS\Edu1stBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use VESS\Edu1stBundle\Entity\CurrCountry;
use VESS\Edu1stBundle\Form\CurrCountryType;

/**
 * CurrCountry controller.
 *
 */
class CurrCountryController extends Controller
{

    /**
     * Lists all CurrCountry entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('VESSEdu1stBundle:CurrCountry')->findAll();

        $multilenguage = $em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue();

        return $this->render('VESSEdu1stBundle:CurrCountry:index.html.twig', array(
            'entities' => $entities,
            'multilenguage' => $multilenguage
        ));
    }
    /**
     * Creates a new CurrCountry entity.
     *
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = new CurrCountry();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('currcountry_show', array('id' => $entity->getId())));
        }

        $multilenguage = $em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue();

        return $this->render('VESSEdu1stBundle:CurrCountry:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'multilenguage' => $multilenguage
        ));
    }

    /**
     * Creates a form to create a CurrCountry entity.
     *
     * @param CurrCountry $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(CurrCountry $entity)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(new CurrCountryType($em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue()), $entity, array(
            'action' => $this->generateUrl('currcountry_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new CurrCountry entity.
     *
     */
    public function newAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entity = new CurrCountry();
        $form   = $this->createCreateForm($entity);

        $multilenguage = $em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue();

        return $this->render('VESSEdu1stBundle:CurrCountry:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'multilenguage' => $multilenguage
        ));
    }

    /**
     * Finds and displays a CurrCountry entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('VESSEdu1stBundle:CurrCountry')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CurrCountry entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        $multilenguage = $em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue();

        return $this->render('VESSEdu1stBundle:CurrCountry:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
            'multilenguage' => $multilenguage
        ));
    }

    /**
     * Displays a form to edit an existing CurrCountry entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('VESSEdu1stBundle:CurrCountry')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CurrCountry entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        $multilenguage = $em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue();

        return $this->render('VESSEdu1stBundle:CurrCountry:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'multilenguage' => $multilenguage
        ));
    }

    /**
    * Creates a form to edit a CurrCountry entity.
    *
    * @param CurrCountry $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(CurrCountry $entity)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(new CurrCountryType($em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue()), $entity, array(
            'action' => $this->generateUrl('currcountry_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing CurrCountry entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('VESSEdu1stBundle:CurrCountry')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CurrCountry entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('currcountry_edit', array('id' => $id)));
        }

        $multilenguage = $em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish')->getValue();

        return $this->render('VESSEdu1stBundle:CurrCountry:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'multilenguage' => $multilenguage
        ));
    }
    /**
     * Deletes a CurrCountry entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('VESSEdu1stBundle:CurrCountry')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find CurrCountry entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('currcountry'));
    }

    /**
     * Creates a form to delete a CurrCountry entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('currcountry_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}

<?php

namespace VESS\Edu1stBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{
    private $user;

    function __construct($user=null)
    {
        $this->user = $user;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $rols = array(
            'ROLE_ADMIN' => 'Global Admin',
            'ROLE_SCHOOL_ADMIN' => 'School Admin',
            'ROLE_SCHOOL_SUPERVISOR' => 'School Supervisor',
            'ROLE_SCHOOL_TEACHER' => 'School Teacher',
        );

        $builder
            ->add('fullname', null, array('label' => 'fullname'))
            ->add('password', 'password', array('required' => false, 'label' => 'change_passwd'))
            ->add('photo', null, array('label' => 'photo'))
            ->add('phone', null, array('label' => 'phone'))
            ->add('email', null, array('label' => 'email'));

        if ($this->user->getRol() == 'ROLE_ADMIN') {
            $builder
                ->add('rol', 'choice', array('choices' => $rols))
            ;
        }
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'VESS\Edu1stBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'vess_edu1stbundle_user';
    }
}

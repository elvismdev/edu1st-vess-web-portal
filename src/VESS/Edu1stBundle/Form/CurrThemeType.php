<?php

namespace VESS\Edu1stBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CurrThemeType extends AbstractType
{
    private $spanish;

    function __construct($spanish = 0)
    {
        $this->spanish = $spanish;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array('label' => 'name',
                'attr' => array('class' => 'form-control', 'style' => 'margin-bottom: 15px'),
            ));

        if ($this->spanish) {
            $builder->add('name_es', 'text', array('label' => 'namees',
                'attr' => array('class' => 'form-control', 'style' => 'margin-bottom: 15px'),
            ));
        }
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'VESS\Edu1stBundle\Entity\CurrTheme'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'vess_edu1stbundle_currtheme';
    }
}

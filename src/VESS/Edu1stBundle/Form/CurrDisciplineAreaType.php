<?php

namespace VESS\Edu1stBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CurrDisciplineAreaType extends AbstractType
{
    private $em;

    private $spanish;

    function __construct($em = null, $spanish = 0)
    {
        $this->em = $em;

        $spanish = $this->em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish');
        $this->spanish = $spanish->getValue();
    }

    function getList()
    {
        $ds = $this->em->getRepository('VESSEdu1stBundle:CurrDomain')->findAll();

        $list = array();
        foreach ($ds as $d) {
            $query = $this->em->createQuery('SELECT ag FROM VESSEdu1stBundle:CurrAgeGroup ag
                WHERE ag.domains = :did1 OR ag.domains LIKE :did2 OR ag.domains LIKE :did3 OR ag.domains LIKE :did4')
                ->setParameter('did1', '[' . $d->getId() . ']')
                ->setParameter('did2', '[' . $d->getId() . ',%')
                ->setParameter('did3', '%,' . $d->getId() . ',%')
                ->setParameter('did4', '%,' . $d->getId() . ']');
            $ages = $query->getResult();

            foreach ($ages as $ag) {
                $list[$d->getName()][$ag->getId() . '-' . $d->getId()] = $ag->getName();
            }
        }

        return $list;
    }

    /*function getList() {
        $ags = $this->em->getRepository('VESSEdu1stBundle:CurrAgeGroup')->findAll();

        $list = array();
        foreach ($ags as $ag) {
            $ds = json_decode($ag->getDomains());
            foreach ($ds as $d) {
                $d = $this->em->getRepository('VESSEdu1stBundle:CurrDomain')->find($d);
                if ($d)
                    $list[$ag->getName()][$ag->getId() . '-' . $d->getId()] = $d->getName();
            }
        }

        return $list;
    }*/

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array( 'label' => 'name',
                'attr' => array('class' => 'form-control', 'style' => 'margin-bottom: 15px'),
            ));

        if ($this->spanish) {
            $builder->add('name_es', 'text', array( 'label' => 'namees',
                'attr' => array('class' => 'form-control', 'style' => 'margin-bottom: 15px'),
            ));
        }

        $builder->add('currAgeGroupDomain', 'choice', array(
                'label' => 'age_group_and_domain',
                'multiple' => true,
                'choices' => $this->getList(),
                'attr' => array('class' => 'form-control', 'size' => '20', 'style' => 'margin-bottom: 15px')
            )
        );
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'VESS\Edu1stBundle\Entity\CurrDisciplineArea'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'vess_edu1stbundle_currdisciplinearea';
    }
}

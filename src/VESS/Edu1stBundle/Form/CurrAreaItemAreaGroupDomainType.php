<?php

namespace VESS\Edu1stBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CurrAreaItemAreaGroupDomainType extends AbstractType
{
    private $em;

    function __construct($em=null)
    {
        $this->em = $em;
    }

    function getList() {
        $ags = $this->em->getRepository('VESSEdu1stBundle:CurrAgeGroup')->findAll();

        $list = array();
        foreach ($ags as $ag) {
            $ds = json_decode($ag->getDomains());
            foreach ($ds as $d) {
                $d = $this->em->getRepository('VESSEdu1stBundle:CurrDomain')->find($d);
                if ($d) {
                    $query = $this->em->createQuery('SELECT daag FROM VESSEdu1stBundle:CurrDisciplineAreaAreaGroup daag
                        WHERE daag.currAgeGroup = :ag AND daag.currDomain = :d')
                        ->setParameter('ag', $ag->getId())
                        ->setParameter('d', $d->getId());
                    $ag->relations = $query->getResult();
                    foreach ($ag->relations as $r) {
                        $discipline = $r->getCurrDisciplineArea();
                        $list[$d->getName()][$ag->getName()][$d->getId() . '-' . $ag->getId() . '-' . $discipline->getId()] = $discipline->getName();
                    }
                }
            }
        }

        return $list;
    }
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('currAreaItem', null, array('label' => 'standard'))
            ->add('themes', 'entity', array(
                'label' => 'themes',
                'required'    => false,
                'class' => 'VESSEdu1stBundle:CurrTheme',
                'property' => 'name',
                'multiple' => true,
                'attr' => array('class' => 'form-control', 'size' => '5', 'style' => 'margin-bottom: 15px'),
            ))
            ->add('countries', 'entity', array(
                'label' => 'countries',
                'required'    => false,
                'class' => 'VESSEdu1stBundle:CurrCountry',
                'property' => 'name',
                'multiple' => true,
                'attr' => array('class' => 'form-control', 'size' => '5', 'style' => 'margin-bottom: 15px'),
            ))
            ->add('artisticFocus', 'entity', array(
                'label' => 'artistic_focus',
                'required'    => false,
                'class' => 'VESSEdu1stBundle:CurrArtisticFocus',
                'property' => 'name',
                'multiple' => true,
                'attr' => array('class' => 'form-control', 'size' => '5', 'style' => 'margin-bottom: 15px'),
            ))
            ->add('currDisciplineAreaCascade', 'choice', array(
                'label' => 'Subdomain',
                'choices' => $this->getList(),
                'multiple' => true,
                'attr' => array('class' => 'form-control', 'size' => '20', 'style' => 'margin-bottom: 15px'),
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'VESS\Edu1stBundle\Entity\CurrAreaItemAreaGroupDomain'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'vess_edu1stbundle_currareaitemareagroupdomain';
    }
}

<?php

namespace VESS\Edu1stBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CurrAreaItemType extends AbstractType
{
    private $em;

    private $spanish;

    private $mode;

    function __construct($em=null, $mode='new')
    {
        $this->em = $em;

        $spanish = $this->em->getRepository('VESSEdu1stBundle:Config')->findOneByParameter('spanish');
        $this->spanish = $spanish->getValue();

        $this->mode = $mode;
    }

    function getList() {
        $ags = $this->em->getRepository('VESSEdu1stBundle:CurrAgeGroup')->findAll();

        $list = array();
        foreach ($ags as $ag) {
            $ds = json_decode($ag->getDomains());
            foreach ($ds as $d) {
                $d = $this->em->getRepository('VESSEdu1stBundle:CurrDomain')->find($d);
                if ($d) {
                    $query = $this->em->createQuery('SELECT daag FROM VESSEdu1stBundle:CurrDisciplineAreaAreaGroup daag
                        WHERE daag.currAgeGroup = :ag AND daag.currDomain = :d')
                        ->setParameter('ag', $ag->getId())
                        ->setParameter('d', $d->getId());
                    $ag->relations = $query->getResult();
                    foreach ($ag->relations as $r) {
                        $discipline = $r->getCurrDisciplineArea();
                        $list[$d->getName()][$ag->getName()][$d->getId() . '-' . $ag->getId() . '-' . $discipline->getId()] = $discipline->getName();
                    }
                }
            }
        }

        return $list;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $attr = array('class' => 'form-control', 'style' => 'margin-bottom: 15px');
        if ($this->mode == 'edit') $attr = array('class' => 'form-control', 'style' => 'margin-bottom: 15px', 'readonly' => true);

        $builder
            ->add('name', 'text', array('label' => 'name',
                'attr' => $attr
            ));

        if ($this->spanish) {
            $builder->add('name_es', 'text', array('label' => 'namees',
                'attr' => $attr
            ));
        }

        if ($this->mode == 'edit') {
            $builder->add('themes')->add('themes', 'entity', array(
                    'label' => 'themes',
                    'required'    => false,
                    'class' => 'VESSEdu1stBundle:CurrTheme',
                    'property' => 'name',
                    'multiple' => true,
                    'attr' => array('class' => 'form-control', 'size' => '5', 'style' => 'margin-bottom: 15px'),
                ))
            ->add('countries', 'entity', array(
                'label' => 'countries',
                'required'    => false,
                'class' => 'VESSEdu1stBundle:CurrCountry',
                'property' => 'name',
                'multiple' => true,
                'attr' => array('class' => 'form-control', 'size' => '5', 'style' => 'margin-bottom: 15px'),
            ))
            ->add('artisticFocus', 'entity', array(
                'label' => 'artistic_focus',
                'required'    => false,
                'class' => 'VESSEdu1stBundle:CurrArtisticFocus',
                'property' => 'name',
                'multiple' => true,
                'attr' => array('class' => 'form-control', 'size' => '5', 'style' => 'margin-bottom: 15px'),
            ))
            ->add('currDisciplineAreaCascade', 'choice', array(
                'label' => 'Subdomain',
                'choices' => $this->getList(),
                'attr' => array('size' => '20', 'style' => 'margin-bottom: 15px'),
            ));
        }
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'VESS\Edu1stBundle\Entity\CurrAreaItem'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'vess_edu1stbundle_currareaitem';
    }
}

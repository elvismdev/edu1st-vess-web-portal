<?php

namespace VESS\Edu1stBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FaqListType extends AbstractType
{
    private $spanish;

    function __construct($spanish = 0)
    {
        $this->spanish = $spanish;
    }

        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('faqCat', null, array('label' => 'faqCat'))
            ->add('question', null, array('label' => 'question'))
            ->add('answer', null, array('label' => 'answer'));

        if ($this->spanish) {
            $builder
                ->add('questiones', null, array('label' => 'questions'))
                ->add('answeres', null, array('label' => 'answers'));
        }
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'VESS\Edu1stBundle\Entity\FaqList'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'vess_edu1stbundle_faqlist';
    }
}

<?php

namespace VESS\Edu1stBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FaqCategories
 */
class FaqCategories
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $namees;

    /**
     * @var string
     */
    private $catIcon;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set catNameEng
     *
     * @param string $name
     * @return FaqCategories
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set namees
     *
     * @param string $namees
     * @return FaqCategories
     */
    public function setNamees($namees)
    {
        $this->namees = $namees;

        return $this;
    }

    /**
     * Get namees
     *
     * @return string 
     */
    public function getNamees()
    {
        return $this->namees;
    }

    /**
     * Set catIcon
     *
     * @param string $catIcon
     * @return FaqCategories
     */
    public function setCatIcon($catIcon)
    {
        $this->catIcon = $catIcon;

        return $this;
    }

    /**
     * Get catIcon
     *
     * @return string 
     */
    public function getCatIcon()
    {
        return $this->catIcon;
    }

    public function __toString()
    {
        return $this->name;
    }
}

<?php

namespace VESS\Edu1stBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CurrAgeGroup
 */
class CurrAgeGroup
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    private $namees;

    /**
     * @param mixed $namees
     */
    public function setNamees($namees)
    {
        $this->namees = $namees;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNamees()
    {
        return $this->namees;
    }

    private $domains;


    /**
     * Set id
     *
     * @param integer $id
     * @return CurrAgeGroup
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return CurrAgeGroup
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $domains
     */
    public function setDomains($domains)
    {
        $this->domains = $domains;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDomains()
    {
        return $this->domains;
    }

    public function __toString()
    {
        return $this->name;
    }
}

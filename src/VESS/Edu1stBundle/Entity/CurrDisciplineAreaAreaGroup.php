<?php

namespace VESS\Edu1stBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CurrDisciplineAreaAreaGroup
 */
class CurrDisciplineAreaAreaGroup
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \VESS\Edu1stBundle\Entity\CurrDisciplineArea
     */
    private $currDisciplineArea;

    /**
     * @var \VESS\Edu1stBundle\Entity\CurrAgeGroup
     */
    private $currAgeGroup;

    /**
     * @var \VESS\Edu1stBundle\Entity\Currdomain
     */
    private $currDomain;

    function __construct(\VESS\Edu1stBundle\Entity\CurrDisciplineArea $currDisciplineArea, \VESS\Edu1stBundle\Entity\CurrAgeGroup $currAgeGroup, \VESS\Edu1stBundle\Entity\Currdomain $currDomain)
    {
        $this->currDisciplineArea = $currDisciplineArea;
        $this->currAgeGroup = $currAgeGroup;
        $this->currDomain = $currDomain;
    }

    /**
     * Set id
     *
     * @param integer $id
     * @return CurrDisciplineAreaAreaGroup
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set currDisciplineArea
     *
     * @param \VESS\Edu1stBundle\Entity\CurrDisciplineArea $currDisciplineArea
     * @return CurrDisciplineAreaAreaGroup
     */
    public function setCurrDisciplineArea(\VESS\Edu1stBundle\Entity\CurrDisciplineArea $currDisciplineArea = null)
    {
        $this->currDisciplineArea = $currDisciplineArea;

        return $this;
    }

    /**
     * Get currDisciplineArea
     *
     * @return \VESS\Edu1stBundle\Entity\CurrDisciplineArea
     */
    public function getCurrDisciplineArea()
    {
        return $this->currDisciplineArea;
    }

    /**
     * Set currAgeGroup
     *
     * @param \VESS\Edu1stBundle\Entity\CurrAgeGroup $currAgeGroup
     * @return CurrDisciplineAreaAreaGroup
     */
    public function setCurrAgeGroup(\VESS\Edu1stBundle\Entity\CurrAgeGroup $currAgeGroup = null)
    {
        $this->currAgeGroup = $currAgeGroup;

        return $this;
    }

    /**
     * Get currAgeGroup
     *
     * @return \VESS\Edu1stBundle\Entity\CurrAgeGroup
     */
    public function getCurrAgeGroup()
    {
        return $this->currAgeGroup;
    }

    /**
     * @param \VESS\Edu1stBundle\Entity\Currdomain $currDomain
     */
    public function setCurrDomain($currDomain)
    {
        $this->currDomain = $currDomain;
    }

    /**
     * @return \VESS\Edu1stBundle\Entity\Currdomain
     */
    public function getCurrDomain()
    {
        return $this->currDomain;
    }

}

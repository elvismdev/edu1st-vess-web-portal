<?php

namespace VESS\Edu1stBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Usuario
 */
class User implements UserInterface {

    /**
     * @var integer
     */
    protected $id;

    protected $username;
    protected $fullname;
    protected $password;
    protected $photo;
    protected $phone;
    protected $email;
    protected $code;
    protected $lastupdate;
    protected $salt;
    protected $rol;

    public function getUsername() {
        return $this->email;
    }

    public function getNickname() {
        return $this->username;
    }

    /**
     * @inheritDoc
     */
    public function getSalt() {
        return $this->salt;
    }

    /**
     * @inheritDoc
     */
    public function getPassword() {
        return $this->password;
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials() {

    }

    public function getFullname() {
        return $this->fullname;
    }

    public function getPhoto() {
        return $this->photo;
    }

    public function getPhone() {
        return $this->phone;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getCode() {
        return $this->code;
    }

    public function getLastupdate() {
        return $this->lastupdate;
    }

    public function getRoles() {
        return array($this->rol);
    }

    public function getRol() {
        return $this->rol;
    }

    /**
     * @inheritDoc
     */
    public function equals(UserInterface $user) {
        return $this->username === $user->getUsername();
    }

    public function setUsername($user) {
        $this->username = $user;
    }

    public function setPassword($pass) {
        $this->password = $pass;
    }

    public function setSalt($salt) {
        $this->salt = $salt;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    public function setId(\VESS\Edu1stBundle\Entity\User $id = null) {
        $this->id = $id;

        return $this;
    }

    public function setFullname($fullname)
    {
        $this->fullname = $fullname;

        return $this;
    }

    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    public function setLastupdate($lastupdate)
    {
        $this->lastupdate = $lastupdate;

        return $this;
    }

    public function setRoles($rol)
    {
        $this->rol = $rol;

        return $this;
    }

    public function setRol($rol)
    {
        $this->rol = $rol;

        return $this;
    }

    public function __toString() {
        return $this->username;
    }

    public function isPasswordLegal()
    {
        return strstr($this->password, $this->username);
    }
}

<?php

namespace VESS\Edu1stBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FaqList
 */
class FaqList
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $question;

    /**
     * @var string
     */
    private $answer;

    /**
     * @var string
     */
    private $questiones;

    /**
     * @var string
     */
    private $answeres;

    /**
     * @var \VESS\Edu1stBundle\Entity\FaqCategories
     */
    private $faqCat;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $answer
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * @return string
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * @param string $answeres
     */
    public function setAnsweres($answeres)
    {
        $this->answeres = $answeres;

        return $this;
    }

    /**
     * @return string
     */
    public function getAnsweres()
    {
        return $this->answeres;
    }

    /**
     * @param string $question
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * @param string $questiones
     */
    public function setQuestiones($questiones)
    {
        $this->questiones = $questiones;

        return $this;
    }

    /**
     * @return string
     */
    public function getQuestiones()
    {
        return $this->questiones;
    }

    /**
     * Set faqCat
     *
     * @param \VESS\Edu1stBundle\Entity\FaqCategories $faqCat
     * @return FaqList
     */
    public function setFaqCat(\VESS\Edu1stBundle\Entity\FaqCategories $faqCat = null)
    {
        $this->faqCat = $faqCat;

        return $this;
    }

    /**
     * Get faqCat
     *
     * @return \VESS\Edu1stBundle\Entity\FaqCategories 
     */
    public function getFaqCat()
    {
        return $this->faqCat;
    }
}

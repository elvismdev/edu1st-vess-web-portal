<?php

namespace VESS\Edu1stBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CurrAreaItem
 */
class CurrAreaItem
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    private $namees;

    private $themes;

    private $countries;

    private $artisticfocus;

    private $currDisciplineAreaCascade;

    /**
     * @param mixed $currDisciplineAreaCascade
     */
    public function setCurrDisciplineAreaCascade($currDisciplineAreaCascade)
    {
        $this->currDisciplineAreaCascade = $currDisciplineAreaCascade;
    }

    /**
     * @return mixed
     */
    public function getCurrDisciplineAreaCascade()
    {
        return $this->currDisciplineAreaCascade;
    }

    /**
     * @param mixed $artisticfocus
     */
    public function setArtisticfocus($artisticfocus)
    {
        $this->artisticfocus = $artisticfocus;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getArtisticfocus()
    {
        return $this->artisticfocus;
    }

    /**
     * @param mixed $countries
     */
    public function setCountries($countries)
    {
        $this->countries = $countries;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCountries()
    {
        return $this->countries;
    }

    /**
     * @param mixed $themes
     */
    public function setThemes($themes)
    {
        $this->themes = $themes;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getThemes()
    {
        return $this->themes;
    }

    /**
     * @param mixed $namees
     */
    public function setNamees($namees)
    {
        $this->namees = $namees;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNamees()
    {
        return $this->namees;
    }

    /**
     * Set id
     *
     * @param integer $id
     * @return CurrAreaItem
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return CurrAreaItem
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    public function __toString()
    {
        return $this->name;
    }
}

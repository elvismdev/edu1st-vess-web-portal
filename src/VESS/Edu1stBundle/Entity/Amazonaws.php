<?php

namespace VESS\Edu1stBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Amazonaws
 */
class Amazonaws
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $awsId;

    /**
     * @var string
     */
    private $awsSecretKey;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set awsId
     *
     * @param string $awsId
     * @return Amazonaws
     */
    public function setAwsId($awsId)
    {
        $this->awsId = $awsId;

        return $this;
    }

    /**
     * Get awsId
     *
     * @return string
     */
    public function getAwsId()
    {
        return $this->awsId;
    }

    /**
     * Set awsSecretKey
     *
     * @param string $awsSecretKey
     * @return Amazonaws
     */
    public function setAwsSecretKey($awsSecretKey)
    {
        $this->awsSecretKey = $awsSecretKey;

        return $this;
    }

    /**
     * Get awsSecretKey
     *
     * @return string
     */
    public function getAwsSecretKey()
    {
        return $this->awsSecretKey;
    }
}

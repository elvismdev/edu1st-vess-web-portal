<?php

namespace VESS\Edu1stBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CurrDisciplineArea
 */
class CurrDisciplineArea
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    private $currAgeGroupDomain;

    /**
     * @param mixed $currAgeGroupDomain
     */
    public function setCurrAgeGroupDomain($currAgeGroupDomain)
    {
        $this->currAgeGroupDomain = $currAgeGroupDomain;
    }

    /**
     * @return mixed
     */
    public function getCurrAgeGroupDomain()
    {
        return $this->currAgeGroupDomain;
    }

    /**
     * Set id
     *
     * @param integer $id
     * @return CurrDisciplineArea
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return CurrDisciplineArea
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    public function __toString()
    {
        return $this->name;
    }
    /**
     * @var string
     */
    private $nameEs;


    /**
     * Set nameEs
     *
     * @param string $nameEs
     * @return CurrDisciplineArea
     */
    public function setNameEs($nameEs)
    {
        $this->nameEs = $nameEs;

        return $this;
    }

    /**
     * Get nameEs
     *
     * @return string
     */
    public function getNameEs()
    {
        return $this->nameEs;
    }
}

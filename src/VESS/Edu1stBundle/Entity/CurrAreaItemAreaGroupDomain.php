<?php

namespace VESS\Edu1stBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CurrAreaItemAreaGroupDomain
 */
class CurrAreaItemAreaGroupDomain
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \VESS\Edu1stBundle\Entity\CurrAreaItem
     */
    private $currAreaItem;

    /**
     * @var \VESS\Edu1stBundle\Entity\CurrAgeGroup
     */
    private $currAgeGroup;

    /**
     * @var \VESS\Edu1stBundle\Entity\CurrDomain
     */
    private $currDomain;

    /**
     * @var \VESS\Edu1stBundle\Entity\CurrDisciplineArea
     */
    private $currDisciplineArea;

    function __construct(\VESS\Edu1stBundle\Entity\CurrDomain $currDomain=null, \VESS\Edu1stBundle\Entity\CurrAgeGroup $currAgeGroup=null, \VESS\Edu1stBundle\Entity\CurrDisciplineArea $currDisciplineArea=null, \VESS\Edu1stBundle\Entity\CurrAreaItem $currAreaItem=null,
                        $themes=null, $countries=null, $artisticFocus=null)
    {
        $this->currAgeGroup = $currAgeGroup;
        $this->currDisciplineArea = $currDisciplineArea;
        $this->currDomain = $currDomain;
        $this->currAreaItem = $currAreaItem;
        $this->themes = $themes;
        $this->countries = $countries;
        $this->artisticFocus = $artisticFocus;
    }

    private $currDisciplineAreaCascade;

    /**
     * @param mixed $currDisciplineAreaCascade
     */
    public function setCurrDisciplineAreaCascade($currDisciplineAreaCascade)
    {
        $this->currDisciplineAreaCascade = $currDisciplineAreaCascade;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCurrDisciplineAreaCascade()
    {
        return $this->currDisciplineAreaCascade;
    }

    /**
     * Set id
     *
     * @param integer $id
     * @return CurrAreaItemAreaGroupDomain
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set currAreaItem
     *
     * @param \VESS\Edu1stBundle\Entity\CurrAreaItem $currAreaItem
     * @return CurrAreaItemAreaGroupDomain
     */
    public function setCurrAreaItem(\VESS\Edu1stBundle\Entity\CurrAreaItem $currAreaItem = null)
    {
        $this->currAreaItem = $currAreaItem;

        return $this;
    }

    /**
     * Get currAreaItem
     *
     * @return \VESS\Edu1stBundle\Entity\CurrAreaItem
     */
    public function getCurrAreaItem()
    {
        return $this->currAreaItem;
    }

    /**
     * Set currAgeGroup
     *
     * @param \VESS\Edu1stBundle\Entity\CurrAgeGroup $currAgeGroup
     * @return CurrAreaItemAreaGroupDomain
     */
    public function setCurrAgeGroup(\VESS\Edu1stBundle\Entity\CurrAgeGroup $currAgeGroup = null)
    {
        $this->currAgeGroup = $currAgeGroup;

        return $this;
    }

    /**
     * Get currAgeGroup
     *
     * @return \VESS\Edu1stBundle\Entity\CurrAgeGroup
     */
    public function getCurrAgeGroup()
    {
        return $this->currAgeGroup;
    }

    /**
     * Set currDomain
     *
     * @param \VESS\Edu1stBundle\Entity\CurrDomain $currDomain
     * @return CurrAreaItemAreaGroupDomain
     */
    public function setCurrDomain(\VESS\Edu1stBundle\Entity\CurrDomain $currDomain = null)
    {
        $this->currDomain = $currDomain;

        return $this;
    }

    /**
     * Get currDomain
     *
     * @return \VESS\Edu1stBundle\Entity\CurrDomain
     */
    public function getCurrDomain()
    {
        return $this->currDomain;
    }

    /**
     * Set currDisciplineArea
     *
     * @param \VESS\Edu1stBundle\Entity\CurrDisciplineArea $currDisciplineArea
     * @return CurrAreaItemAreaGroupDomain
     */
    public function setCurrDisciplineArea(\VESS\Edu1stBundle\Entity\CurrDisciplineArea $currDisciplineArea = null)
    {
        $this->currDisciplineArea = $currDisciplineArea;

        return $this;
    }

    /**
     * Get currDisciplineArea
     *
     * @return \VESS\Edu1stBundle\Entity\CurrDisciplineArea
     */
    public function getCurrDisciplineArea()
    {
        return $this->currDisciplineArea;
    }
    /**
     * @var string
     */
    private $themes;

    /**
     * @var string
     */
    private $countries;

    /**
     * @var string
     */
    private $artisticFocus;


    /**
     * Set themes
     *
     * @param string $themes
     * @return CurrAreaItemAreaGroupDomain
     */
    public function setThemes($themes)
    {
        $this->themes = $themes;

        return $this;
    }

    /**
     * Get themes
     *
     * @return string 
     */
    public function getThemes()
    {
        return $this->themes;
    }

    /**
     * Set countries
     *
     * @param string $countries
     * @return CurrAreaItemAreaGroupDomain
     */
    public function setCountries($countries)
    {
        $this->countries = $countries;

        return $this;
    }

    /**
     * Get countries
     *
     * @return string 
     */
    public function getCountries()
    {
        return $this->countries;
    }

    /**
     * Set artisticFocus
     *
     * @param string $artisticFocus
     * @return CurrAreaItemAreaGroupDomain
     */
    public function setArtisticFocus($artisticFocus)
    {
        $this->artisticFocus = $artisticFocus;

        return $this;
    }

    /**
     * Get artisticFocus
     *
     * @return string 
     */
    public function getArtisticFocus()
    {
        return $this->artisticFocus;
    }
}
